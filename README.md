MulitServicer由以下程序组成：
1、通用服务器wefeelServer.exe：主程序，即可直接执行(运行wefeelserver.exe)，也可作为服务执行，还可配置服务(设置WefeelServer服务.bat)。
2、通用短信插件SMSPlugin.dll：短信收发，通用型数据库，串口通信。
3、通达串口短信插件TDOA_SMS.dll：短信收发，通达数据库，串口通信，可通过7731端口发送监视信息。
4、监视通达短信自动插件MonitorAutoPlugin.dll：由WefeelServer.exe自动加载，可在WefeelServer的主界面菜单上调用，可监视TDOA_SMS.dll的信息。

MultiServicer综合服务器开发了一个通用的、可加载dll插件的、可作为服务运行的主程序和若干插件。
一、    主程序－通用服务器
WefeelServer.exe是个主程序，既可作为windows服务使用，也可直接运行使用。集成了服务控制界面。配置文件WefeelServer.ini，主要配置其加载的插件。WefeelServer无具体功能，功能由两类插件实现。A类插件是由ini配置的插件，最多限制20个，可以自动运行；B类插件是自动检测的插件，检测到后放入菜单，需要手动运行。
WefeelServer /setup进入服务控制界面，可对服务进行安装、卸载、启动、停止等控制。
作为服务运行时，服务的命令是WefeelServer /serivce，/service参数用于告诉程序这是以windows服务方式运行。
WefeelServer不带参数运行时，作为一般windows程序运行。
WefeelServer应用范围较广，可不限于短信网关。
二、	A类插件－服务器插件
a)	规范
A类插件通过主程序WefeelServer.ini文件配置加载，要求实现以下函数。至少实现Start才能被认为是符合规范的A类插件。
  function Start():boolean;stdcall; //开始运行插件，必须实现才能被主程序通过ini文件调用
  function Stop():boolean;stdcall;  //停止运行插件，可选
  function Setup():boolean;stdcall; //进行插件设置，可选
  function About():boolean;stdcall; //显示插件版本信息，可选
  function IsRun():boolean;stdcall; //返回插件是否正在运行，可选
  function Show():boolean;stdcall;  //显示插件主窗口，可选
b)	短信网关插件－SMSPlugin.dll
WefeelServer可加载短信网关插件SMSPlugin.dll作为短信网关使用。SMSPlugin.dll插件的主窗口通过定时器实现轮询，并调用WefeelSMS.dll对短信设备进行控制。参数配置文件与dll文件名相同。该插件可复制后在WefeelServer.ini中多次调用，可满足控制多个短信设备需要。
SMSPlugin.dll用到WefeelSMS.dll动态库，实现对短信设备的操作，可实现短信收发、自动发送超长短信、短信转发等功能。由于多个SMSPlugin.dll调用同一个WefeelSMS.dll时实际在内存中只加载一次，因此在需要运行多个SMSPlugin.dll时（如移动和联通需要分别网内发送），SMSPlugin.dll和WefeelSMS.dll应放在多个目录下分别加载。
三、	B类插件－自动插件
	B类插件放在目录“插件”下，编写规则见AutoPlugin.pas。例程参见MonitorAutoPlugin.exe。
四、	经验：
Dll插件中不能和宿主程序共用一个ADO连接，必须另外建立连接。
Dll和exe对form中控件的初始化不完全一样，dll中线程调用主窗口的memo控件前，需要先在主窗口中给Memo中提前增加一行才行。Exe不存在以上问题。
用到ado的dll需要CoInitialize(nil)和CoUninitialize，特别是在没有form的dll中一定需要。

