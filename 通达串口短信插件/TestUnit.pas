unit TestUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask;

type
  TTestForm = class(TForm)
    Label1: TLabel;
    edtCode: TEdit;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    mmoMessage: TMemo;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TestForm: TTestForm;

implementation

uses MonitorUnit;

{$R *.dfm}

procedure TTestForm.BitBtn1Click(Sender: TObject);
begin
  if Trim(edtCode.Text)<>'' then
  begin
    MonitorForm.qrySend.Open;
    if MonitorForm.qrySend.Active then
    begin
      MonitorForm.qrySend.Append;
      MonitorForm.qrySend.FieldByName('����').Value:=Trim(edtCode.Text);
      MonitorForm.qrySend.FieldByName('����').Value:=mmoMessage.Lines.Text;
      MonitorForm.qrySend.FieldByName('��Դ').Value:='����';
      MonitorForm.qrySend.Post;
    end;
    MonitorForm.qrySend.Close;
  end;
end;

procedure TTestForm.BitBtn2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TTestForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  TestForm:=nil;
end;

end.
