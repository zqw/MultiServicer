unit ServiceUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  ActiveX,Forms;

type
  TSMSService = class(TService)  //服务的类名必须和服务名一致 SMSService
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  SMSService: TSMSService;

implementation

uses  MonitorUnit;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  SMSService.Controller(CtrlCode);
end;

function TSMSService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TSMSService.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  while not Terminated do
  begin
    Sleep(30);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TSMSService.ServiceExecute(Sender: TService);
begin
  while not Terminated do
  begin
    Sleep(30);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TSMSService.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Paused := True;
end;

procedure TSMSService.ServiceShutdown(Sender: TService);
begin
  MonitorForm.Close;
  MonitorForm.Free;
  Status := csStopped;
  ReportStatus();
end;

procedure TSMSService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  CoInitialize(nil);
  Svcmgr.Application.CreateForm(TMonitorForm, MonitorForm);
  MonitorForm.HelpAction.Visible:=false;
  MonitorForm.RzTrayIcon1.MinimizeApp;
  Started := True;
end;

procedure TSMSService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  MonitorForm.Close;
  MonitorForm.Free;
  CoUnInitialize;
  Stopped := True;
end;

end.
