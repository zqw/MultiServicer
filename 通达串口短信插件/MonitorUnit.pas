
unit MonitorUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus,
  ToolWin, Buttons,IniFiles,StrUtils, //DB,ADODB,
  ActnList, ImgList,ShellAPI,mysql,ActiveX, Sockets, ScktComp;

type
  TMonitorForm = class(TForm)
    Timer1: TTimer;
    Memo1: TMemo;
    CoolBar1: TCoolBar;
    StatusBar: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    StartAction: TAction;
    StopAction: TAction;
    TestAction: TAction;
    SettingAction: TAction;
    HelpAction: TAction;
    AboutAction: TAction;
    ExitAction: TAction;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    DataImageList: TImageList;
    StatisticalAction: TAction;
    ToolButton3: TToolButton;
    ToolButton12: TToolButton;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    N5: TMenuItem;
    ToolButton6: TToolButton;
    PluginsAction: TAction;
    ReceiveTimer: TTimer;
    ResultTimer: TTimer;
    ServerSocket1: TServerSocket;
    procedure Timer1Timer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure TestActionExecute(Sender: TObject);
    procedure SettingActionExecute(Sender: TObject);
    procedure StartActionExecute(Sender: TObject);
    procedure StopActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ReceiveTimerTimer(Sender: TObject);
    procedure ResultTimerTimer(Sender: TObject);
    procedure ServerSocket1ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket1ClientError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
  private
    procedure DoReceived(Source,Time,Content:string);
  public
    { Public declarations }
    ComPort:integer;
    ComBaud:integer;
    Interval:integer;//发送间隔
    PhoneNumber:string;//本机号码
    //IsRelay:integer;//是否转发
    //RelayNumber:string;//转发号码
    ServiceName:string;//服务名称
    ServiceMemo:string;//服务描述
    SendOKCount:integer;
    SendFailCount:integer;
    ReceiveCount:integer;
    //function Authorize():boolean;

    p_mysql:PMYSQL;
    p_res:PMYSQL_RES;
    p_row: PMYSQL_ROW;
    mysqlhost,mysqluser,mysqlpassword,mysqldatabase:string;
    mysqlport:integer;
    mobile:string;

    procedure MemoMessage(str:string);
  end;

var
  MonitorForm: TMonitorForm;
  smshandle:THandle;
  function GetProductVersion():string;


implementation

uses WefeelSMSDLL, MyAbout, SettingUnit, TestSendUnit;//,DAMPlusAppApis;

{$R *.dfm}                                    

//统一格式显示信息
procedure TMonitorForm.MemoMessage(str:string);
var
  i:integer;
  s:string;
begin
  Memo1.Lines.BeginUpdate;
  if Memo1.Lines.Count>300 then
    Memo1.Lines.Delete(0);
  Memo1.Lines.EndUpdate;
  s:=DateTimeToStr(Now())+'|'+str;
  Memo1.Lines.Add(s);
  //Memo1.Refresh;

  for i:=0 to ServerSocket1.Socket.ActiveConnections-1 do
  begin
    if ServerSocket1.Socket.Connections[i].Data<>nil then
      ServerSocket1.Socket.Connections[i].SendText(s);
  end;
end;

//从Project信息中取版本号并显示在标题
function GetProductVersion():string;
const
  InfoNum = 10;
  InfoStr: array[1..InfoNum] of string = ('CompanyName', 'FileDescription', 'FileVersion', 'InternalName', 'LegalCopyright', 'LegalTradeMarks', 'OriginalFileName', 'ProductName', 'ProductVersion', 'Comments');
var
  S: string;
  n, Len: DWORD;
  Buf: PChar;
  Value: PChar;
begin
  //取系统版本
  S := GetModuleName(HInstance);
  n := GetFileVersionInfoSize(PChar(S), n);
  if n > 0 then
  begin
    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(S), 0, n, Buf);
    if VerQueryValue(Buf, PChar('StringFileInfo\080403A8\' + InfoStr[3]), Pointer(Value), Len) then
    begin
        Result:=Value;
    end;
    FreeMem(Buf, n);
  end;
end;

procedure TMonitorForm.Timer1Timer(Sender: TObject);
var
  row_num:integer;
  sql,context:string;
begin
  try
    //发送短信
    if SMSQueueLength()<10 then
    begin //防止发送队列太多
      //连接数据库
      p_mysql:=nil;
      p_mysql:=mysql_init(nil);
      if( mysql_real_connect(p_mysql,PAnsiChar(mysqlhost),PAnsiChar(mysqluser),PAnsiChar(mysqlpassword),PAnsiChar(mysqldatabase),mysqlport,nil,0)<>p_mysql) then
      begin
        mysql_close(p_mysql);
        Timer1.Interval:=10000;
        raise Exception.Create('连接'+mysqlhost+'的数据库'+mysqldatabase+'错误!');
      end;
      mysql_set_character_set(p_mysql,PAnsiChar('GBK')); //数据库用的GBK编码，中文必须
      //处理待发送的短信
      if Pos('*',mobile)>0 then
        sql:='SELECT * FROM SMS2 WHERE SEND_FLAG=0 LIMIT 1'
      else
        sql:='SELECT * FROM SMS2 WHERE SEND_FLAG=0 AND FIND_IN_SET( SUBSTR( PHONE, 1, 3 ) , "'+mobile+'" ) LIMIT 1';
      if mysql_query(p_mysql,PAnsiChar(sql))<>0 then
      begin
        mysql_close(p_mysql);
        Timer1.Interval:=10000;
        raise Exception.Create('执行sql错误!');
      end;
      p_res:=mysql_store_result(p_mysql);
      row_num:=mysql_num_rows(p_res);
      if( row_num=0 ) then
        Timer1.Interval:=3000
      else
        Timer1.Interval:=1000;
      try
        p_row:=mysql_fetch_row(p_res);
        if p_row<>nil then
        begin
          try
            context:=p_row^[3];
            context:=StringReplace(context,#13#10,'',[rfReplaceAll]);
            SMSPutSMS(PChar(p_row^[2]),PChar(context),StrToInt(p_row^[0]));
            //0待发送 1发送成功 2发送超时 3发送中
            mysql_query(p_mysql,PAnsiChar('UPDATE SMS2 SET SEND_FLAG=3 WHERE SMS_ID='+string(p_row^[0])) );
            MemoMessage('发送|'+string(p_row^[0])+'|'+string(p_row^[1])+'|'+string(p_row^[2])+'|'+context);
          except
            on e:Exception do MemoMessage(e.Message);
          end;
        end;
      finally
        mysql_free_result(p_res);
      end;

      mysql_close(p_mysql);
    end;

  except
    on e:Exception do
    begin
      MemoMessage(e.Message);
    end;
  end;

  //刷新发送队列
  StatusBar.Panels[1].Text := '发送队列：'+IntToStr(SMSQueueLength()) + '条';
end;

procedure TMonitorForm.FormDestroy(Sender: TObject);
begin
  StopActionExecute(nil);
  UnloadSMSDll(smshandle);
  libmysql_free;
  //CoUninitialize;
end;

procedure TMonitorForm.ExitActionExecute(Sender: TObject);
begin
  Close();
end;

procedure TMonitorForm.TestActionExecute(Sender: TObject);
begin
  if not Assigned(TestSendForm) then
    TestSendForm:=TTestSendForm.Create(Application);
  TestSendForm.ShowModal();
end;

procedure TMonitorForm.SettingActionExecute(Sender: TObject);
begin
  if not Assigned(SettingForm) then
    SettingForm:=TSettingForm.Create(Application);
  if SettingForm.ShowModal()=mrOK then
  begin
    if Self.StopAction.Enabled=true then
    begin
      //Self.StopActionExecute(nil);
      Self.StartActionExecute(nil);
    end;
  end;
end;

procedure TMonitorForm.StartActionExecute(Sender: TObject);
var
  ini: TIniFile;
begin
  Self.StopActionExecute(nil);
  
  //读配置文件
  //ini := TIniFile.Create(GetCurrentDir+'\'+'SMSPlugin.INI');
  ini := TIniFile.Create(ChangeFileExt(GetModuleName(HInstance),'.INI'));
  Self.Caption := ini.ReadString('Setup', '程序名称', '通达串口短信网关');
  ComPort:= ini.ReadInteger('SMS', 'ComPort', 1);
  ComBaud:= ini.ReadInteger('SMS', 'ComBaud', 9600);
  Interval:=ini.ReadInteger('SMS','Interval',7);
  PhoneNumber:=ini.ReadString('Setup','本机号码','');
  //IsRelay:= ini.ReadInteger('Setup', '是否转发', 0);
  //RelayNumber:= ini.ReadString('Setup', '转发号码', '');
  mobile := ini.ReadString('Setup', '号段', '*');

  mysqlhost:=ini.ReadString('MYSQL','地址','localhost');
  mysqluser:=ini.ReadString('MYSQL','用户','root');
  mysqlpassword:=ini.ReadString('MYSQL','密码','myoa888');
  mysqldatabase:=ini.ReadString('MYSQL','数据库','TD_OA');
  mysqlport:=ini.ReadInteger('MYSQL','端口',3336);

  ini.Free;
  //程序标题
  Self.Caption:=Self.Caption+' '+GetProductVersion();

  try
    p_mysql:=nil;
    p_mysql:=mysql_init(nil);
    if p_mysql=nil then
    begin
      MemoMessage('mysql初始化错误!');
      Exit;
    end;
    //连接数据库
    if( mysql_real_connect(p_mysql,PAnsiChar(mysqlhost),PAnsiChar(mysqluser),PAnsiChar(mysqlpassword),PAnsiChar(mysqldatabase),mysqlport,nil,0)<>p_mysql) then
    begin
      mysql_close(p_mysql);
      MemoMessage('连接'+mysqlhost+'的数据库'+mysqldatabase+'错误!');
      Exit;
    end;
    MemoMessage('提示|连接'+mysqlhost+'的数据库'+mysqldatabase+'成功');
    mysql_set_character_set(p_mysql,PAnsiChar('GBK')); //数据库用的GBK编码，中文必须
    mysql_close(p_mysql);
  except
    on e:Exception do
    begin
      MemoMessage(e.Message);
    end;
  end;

  SetCurrentDir(ExtractFileDir(Application.ExeName));

  MemoMessage('版本|'+Self.Caption);
  SMSCreate();
  SMSSetting(ComPort,ComBaud);
  try
    SMSStart();
    StatusBar.Panels[0].Text := '连接短信设备成功';
    Timer1.Enabled:=true;
    ReceiveTimer.Enabled:=true;
    ResultTimer.Enabled:=true;
    StartAction.Enabled:=false;
    StopAction.Enabled:=true;
    MemoMessage('提示|打开串口'+IntToStr(ComPort)+'成功');
  except
    StatusBar.Panels[0].Text := '连接短信设备失败';
    MemoMessage('提示|打开串口'+IntToStr(ComPort)+'失败');
    Exit;
  end;
  //Authorize(); //控制开启Timer1

end;

procedure TMonitorForm.StopActionExecute(Sender: TObject);
begin
  SMSStop();
  SMSFree();
  Timer1.Enabled:=false;
  ReceiveTimer.Enabled:=false;
  ResultTimer.Enabled:=false;
  StartAction.Enabled:=true;
  StopAction.Enabled:=false;
  MemoMessage('提示|网关已停止运行！');
end;

procedure TMonitorForm.DoReceived(Source,Time,Content:string);
var
  sql:string;
begin
  try
    p_mysql:=nil;
    p_mysql:=mysql_init(nil);
    if p_mysql=nil then
    begin
      MemoMessage('mysql初始化错误!');
      Exit;
    end;
    //连接数据库
    if( mysql_real_connect(p_mysql,PAnsiChar(mysqlhost),PAnsiChar(mysqluser),PAnsiChar(mysqlpassword),PAnsiChar(mysqldatabase),mysqlport,nil,0)<>p_mysql) then
    begin
      mysql_close(p_mysql);
      MemoMessage('连接'+mysqlhost+'的数据库'+mysqldatabase+'错误!');
      Exit;
    end;
    mysql_set_character_set(p_mysql,PAnsiChar('GBK')); //数据库用的GBK编码，中文必须

    sql:='UPDATE SMS2 SET CONTENT=CONCAT(CONTENT,"</br>'+Time+'回复:'+Content+'")'
        +' WHERE SEND_FLAG=1 AND PHONE="'+Source+'" ORDER BY SMS_ID DESC LIMIT 1';
    mysql_query(p_mysql,PAnsiChar(sql) );

    mysql_close(p_mysql);
  except
    on e:Exception do
    begin
      MemoMessage(e.Message);
      mysql_close(p_mysql);
    end;
  end;

end;

procedure TMonitorForm.AboutActionExecute(Sender: TObject);
begin
  if not Assigned(MyAboutBox) then
    MyAboutBox:=TMyAboutBox.Create(Application);
  MyAboutBox.ShowModal;
end;

procedure TMonitorForm.FormCreate(Sender: TObject);
begin
  //CoInitialize(nil);
  libmysql_load(nil);
  smshandle:=LoadSMSDll();
  StartActionExecute(nil);
end;


procedure TMonitorForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caHide;
end;

procedure TMonitorForm.ReceiveTimerTimer(Sender: TObject);
var
  strCode, strTime, strContent: array[0..500] of char;
  Source,Dest,MessageTime,MessageContent:string;
begin
  //检查是否收到短信
  if SMSGetSMS(@strCode, @strTime, @strContent) then
  begin
    //整理数据
    Source:=Trim(strCode);
    if Length(Source)>11 then
    begin
      if Pos('+86',Source)=1 then
        Delete(Source,1,3);
      if Pos('86',Source)=1 then
        Delete(Source,1,2);
    end;
    Dest:='';
    MessageTime:=Trim(strTime);
    MessageContent:=Trim(strContent);

    Inc(ReceiveCount);
    MemoMessage('接收|'+Source+'|'+MessageContent);

    DoReceived(Source,MessageTime,MessageContent);
    StatusBar.Panels[2].Text := '发送成功：'+IntToStr(SendOKCount) + '条'
      +' | 发送失败：'+IntToStr(SendFailCount) + '条'
      +' | 接收：'+IntToStr(ReceiveCount) + '条';
  end;

end;

procedure TMonitorForm.ResultTimerTimer(Sender: TObject);
var
  id,state:integer;
begin
  try
    //检查发送结果
    if SMSGetState(id,state) then
    begin
      //连接数据库
      p_mysql:=nil;
      p_mysql:=mysql_init(nil);
      if( mysql_real_connect(p_mysql,PAnsiChar(mysqlhost),PAnsiChar(mysqluser),PAnsiChar(mysqlpassword),PAnsiChar(mysqldatabase),mysqlport,nil,0)<>p_mysql) then
      begin
        raise Exception.Create('连接'+mysqlhost+'的数据库'+mysqldatabase+'错误!');
      end;
      mysql_set_character_set(p_mysql,PAnsiChar('GBK')); //数据库用的GBK编码，中文必须

      if state=0 then //发送成功
      begin
        //0待发送 1发送成功 2发送超时 3发送中
        mysql_query(p_mysql,PAnsiChar('UPDATE SMS2 SET SEND_FLAG=1 WHERE SMS_ID='+IntToStr(id) ) );
        Inc(SendOKCount);
        MemoMessage('提示|'+IntToStr(id)+'|发送成功');
      end
      else //发送失败
      begin
        mysql_query(p_mysql,PAnsiChar('UPDATE SMS2 SET SEND_FLAG=2 WHERE SMS_ID='+IntToStr(id) ) );
        Inc(SendFailCount);
        MemoMessage('提示|'+IntToStr(id)+'|发送失败');
      end;
      mysql_close(p_mysql);
      StatusBar.Panels[2].Text := '发送成功：'+IntToStr(SendOKCount) + '条'
        +' | 发送失败：'+IntToStr(SendFailCount) + '条'
        +' | 接收：'+IntToStr(ReceiveCount) + '条';
    end;
  except
    on e:Exception do
    begin
      MemoMessage(e.Message);
    end;
  end;
end;


procedure TMonitorForm.ServerSocket1ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
var
  command:string;
begin
  command:=Socket.ReceiveText;
  if command='iamzong' then
  begin
    Socket.Data:=@MonitorForm;
    Socket.SendText(Memo1.Lines.Text);
  end
  else if command='start' then
    StartActionExecute(nil)
  else if command='stop' then
    StopActionExecute(nil);
end;

procedure TMonitorForm.ServerSocket1ClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  if ErrorCode<>0 then Socket.Close;
  ErrorCode:=0;
end;

initialization
  CoInitialize(nil);
finalization
  CoUninitialize;

end.
