unit MyAbout;

interface

uses Windows, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TMyAboutBox = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MyAboutBox: TMyAboutBox;

implementation

uses MonitorUnit;

{$R *.dfm}

procedure TMyAboutBox.FormCreate(Sender: TObject);
begin
  Version.Caption:=GetProductVersion();
end;

procedure TMyAboutBox.BitBtn1Click(Sender: TObject);
begin
  Close();
end;

procedure TMyAboutBox.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  MyAboutBox:=nil;
end;

end.
 
