object StatisticalForm: TStatisticalForm
  Left = 289
  Top = 275
  Width = 303
  Height = 257
  Caption = #30701#20449#25910#21457#25968#37327#32479#35745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object DBGrid1: TDBGrid
    Left = 0
    Top = 33
    Width = 295
    Height = 194
    Align = alClient
    Ctl3D = True
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #23435#20307
    TitleFont.Style = [fsBold]
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = #26102#38388
        Title.Alignment = taCenter
        Width = 101
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = #21457#36865#25968#37327
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = #25509#25910#25968#37327
        Visible = True
      end>
  end
  object CoolBar1: TCoolBar
    Left = 0
    Top = 0
    Width = 295
    Height = 33
    AutoSize = True
    Bands = <
      item
        Control = RadioGroup1
        ImageIndex = -1
        MinHeight = 33
        Width = 295
      end>
    EdgeInner = esNone
    EdgeOuter = esNone
    object RadioGroup1: TRadioGroup
      Left = 9
      Top = 0
      Width = 282
      Height = 33
      Columns = 2
      Ctl3D = True
      ItemIndex = 0
      Items.Strings = (
        #25353#26376#20221
        #25353#26085#26399)
      ParentCtl3D = False
      TabOrder = 0
      OnClick = RadioGroup1Click
    end
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = ADOQuery1
    Left = 64
    Top = 72
  end
  object ADOQuery1: TADOQuery
    Connection = MonitorForm.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT a.'#26102#38388',b.'#21457#36865#25968#37327',c.'#25509#25910#25968#37327' FROM '
      '(SELECT DISTINCT x.* FROM'
      '(SELECT CONVERT(CHAR(8),CAST('#26102#38388' AS DATETIME),112) AS '#26102#38388
      'FROM td'#21457#36865#26085#24535
      'UNION'
      'SELECT CONVERT(CHAR(8),CAST('#26102#38388' AS DATETIME),112) AS '#26102#38388
      'FROM td'#25509#25910') x) a'
      'LEFT JOIN '
      
        '(SELECT CONVERT(CHAR(8),CAST('#26102#38388' AS DATETIME),112) AS '#26102#38388',COUNT(*)' +
        ' AS '#21457#36865#25968#37327' FROM td'#21457#36865#26085#24535
      
        'GROUP BY CONVERT(CHAR(8),CAST('#26102#38388' AS DATETIME),112)) b ON a.'#26102#38388'=b.' +
        #26102#38388
      'LEFT JOIN '
      
        '(SELECT CONVERT(CHAR(8),CAST('#26102#38388' AS DATETIME),112) AS '#26102#38388',COUNT(*)' +
        ' AS '#25509#25910#25968#37327' FROM td'#25509#25910
      
        'GROUP BY CONVERT(CHAR(8),CAST('#26102#38388' AS DATETIME),112)) c ON a.'#26102#38388'=c.' +
        #26102#38388)
    Left = 112
    Top = 72
  end
end
