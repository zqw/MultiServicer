unit SettingUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, IniFiles;

type
  TSettingForm = class(TForm)
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    ComPort: TComboBox;
    ComBaud: TComboBox;
    Interval: TEdit;
    Label7: TLabel;
    Edit5: TEdit;
    CheckBox1: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SettingForm: TSettingForm;

implementation

uses MonitorUnit;

{$R *.dfm}

procedure TSettingForm.FormShow(Sender: TObject);
var
  ini: TIniFile;
  i:integer;
begin
  //ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  //读配置文件
  ini := TIniFile.Create(ChangeFileExt(GetModuleName(HInstance),'.INI'));
  Edit1.Text := ini.ReadString('Setup', '程序名称', '短信通信网关');
  Edit2.Text := ini.ReadString('Setup', '数据库', 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=SMSBuffer.mdb;Persist Security Info=False');
  Edit3.Text :=ini.ReadString('Setup','SendSQL','SELECT TOP 1 * FROM td发送 ORDER BY ID DESC');
  Edit4.Text :=ini.ReadString('Setup','本机号码','');
  ComPort.Text:= ini.ReadString('SMS', 'ComPort', '1');
  ComBaud.Text:= ini.ReadString('SMS', 'ComBaud', '9600');
  Interval.Text:=ini.ReadString('SMS', 'Interval','7');
  i:=ini.ReadInteger('Setup','是否转发',0);
  if(i=0) then
  begin
    CheckBox1.Checked:=false;
    Edit5.Enabled:=false;
  end
  else
  begin
    CheckBox1.Checked:=true;
    Edit5.Enabled:=true;
  end;
  Edit5.Text :=ini.ReadString('Setup','转发号码','');
  ini.Free;
end;

procedure TSettingForm.BitBtn1Click(Sender: TObject);
var
  ini: TIniFile;
begin
  //ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  ini := TIniFile.Create(GetCurrentDir+'\'+'SMSPlugin.INI');
  ini.WriteString('SMS','ComPort',ComPort.Text);
  ini.WriteString('SMS','ComBaud',ComBaud.Text);
  ini.WriteString('SMS','Interval',Interval.Text);
  ini.WriteString('Setup', '程序名称', Edit1.Text);
  ini.WriteString('Setup', '数据库', Edit2.Text);
  ini.WriteString('Setup','SendSQL',Edit3.Text);
  ini.WriteString('Setup','本机号码',Edit4.Text);
  if( CheckBox1.Checked )then
    ini.WriteString('Setup','是否转发','1')
  else
    ini.WriteString('Setup','是否转发','0');
  ini.WriteString('Setup','转发号码',Edit5.Text);
  ini.Free;

  ModalResult:=mrOK;
end;

procedure TSettingForm.CheckBox1Click(Sender: TObject);
begin
  if( CheckBox1.Checked ) then
    Edit5.Enabled:=true
  else
    Edit5.Enabled:=false;
end;

procedure TSettingForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  SettingForm:=nil;
end;

procedure TSettingForm.BitBtn2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

end.
