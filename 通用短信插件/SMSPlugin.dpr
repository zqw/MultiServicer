library SMSPlugin;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  SMSExports in 'SMSExports.pas',
  MonitorUnit in 'MonitorUnit.pas' {MonitorForm},
  TestSendUnit in 'TestSendUnit.pas' {TestSendForm},
  StatisticalUnit in 'StatisticalUnit.pas' {StatisticalForm},
  MyAbout in 'MyAbout.pas' {MyAboutBox},
  SettingUnit in 'SettingUnit.pas' {SettingForm},
  WefeelSMSDLL in 'WefeelSMSDLL.pas';

exports
  Start,
  Stop,
  Setup,
  About,
  IsRun,
  Show;

{$R *.res}

begin
end.
