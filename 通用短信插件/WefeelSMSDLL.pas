unit WefeelSMSDLL;

interface
  uses Windows,SysUtils,Dialogs;

  {procedure SMSCreate();stdcall;external 'WefeelSMS.dll';
  procedure SMSSetting(ComNumber:integer;Baud:integer);stdcall;external 'WefeelSMS.dll';
  procedure SMSStart();stdcall;external 'WefeelSMS.dll';
  procedure SMSStop();stdcall;external 'WefeelSMS.dll';
  procedure SMSFree();stdcall;external 'WefeelSMS.dll';
  procedure SMSPutSMS(pCode,pContent:PChar;id:integer);stdcall;external 'WefeelSMS.dll';
  function SMSGetSMS(pCode,pTime,pContent:PChar):boolean;stdcall;external 'WefeelSMS.dll';
  function SMSGetLine(pLine:PChar):boolean;stdcall;external 'WefeelSMS.dll';
  function SMSQueueLength():integer;stdcall;external 'WefeelSMS.dll';
  procedure SMSSettingWindow();stdcall;external 'WefeelSMS.dll';
  procedure SMSSetSendSpeed(Speed:integer);stdcall;external 'WefeelSMS.dll';
  function SMSGetSendSpeed():integer;stdcall;external 'WefeelSMS.dll';
  function SMSGetState(out Id,State:integer):boolean;stdcall;external 'WefeelSMS.dll';}
var
  //改用动态方式调用。WefeelSMS.dll有全局变量sms加载一次dll不能两次调用。
  //因静态方式 external 'WefeelSMS.dll'目录是exe所在目录，不是dll所在目录，故多个网关调用wefeelsms.dll均为一次加载。
  //动态方式放在不同的目录下，系统认为是不同的dll，故多个网关多次加载，从而保证全局变量sms不会重。
  SMSCreate:procedure();stdcall;
  SMSSetting:procedure (ComNumber:integer;Baud:integer);stdcall;
  SMSStart:procedure ();stdcall;
  SMSStop:procedure ();stdcall;
  SMSFree:procedure ();stdcall;
  SMSPutSMS:procedure (pCode,pContent:PChar;id:integer);stdcall;
  SMSGetSMS:function (pCode,pTime,pContent:PChar):boolean;stdcall;
  SMSGetLine:function (pLine:PChar):boolean;stdcall;
  SMSQueueLength:function ():integer;stdcall;
  SMSSettingWindow:procedure ();stdcall;
  SMSSetSendSpeed:procedure (Speed:integer);stdcall;
  SMSGetSendSpeed:function ():integer;stdcall;
  SMSGetState:function (out Id,State:integer):boolean;stdcall;

  function LoadSMSDll():THandle;
  procedure UnloadSMSDll(handle:THandle);

implementation

function LoadSMSDll():THandle;
begin
  Result:=LoadLibrary(PChar(Extractfilepath(GetModuleName(hInstance))+'WefeelSMS.dll')); //装载DLL，返回句柄
  if Result=0 then
  begin
    ShowMessage('加载'+Extractfilepath(GetModuleName(hInstance))+'WefeelSMS.dll'+'失败!');
    Exit;
  end;
  @SMSCreate:=GetProcAddress(Result,'SMSCreate');
  @SMSSetting:=GetProcAddress(Result,'SMSSetting');
  @SMSStart:=GetProcAddress(Result,'SMSStart');
  @SMSStop:=GetProcAddress(Result,'SMSStop');
  @SMSFree:=GetProcAddress(Result,'SMSFree');
  @SMSPutSMS:=GetProcAddress(Result,'SMSPutSMS');
  @SMSGetSMS:=GetProcAddress(Result,'SMSGetSMS');
  @SMSGetLine:=GetProcAddress(Result,'SMSGetLine');
  @SMSQueueLength:=GetProcAddress(Result,'SMSQueueLength');
  @SMSSettingWindow:=GetProcAddress(Result,'SMSSettingWindow');
  @SMSSetSendSpeed:=GetProcAddress(Result,'SMSSetSendSpeed');
  @SMSGetSendSpeed:=GetProcAddress(Result,'SMSGetSendSpeed');
  @SMSGetState:=GetProcAddress(Result,'SMSGetState');
end;

procedure UnloadSMSDll(handle:THandle);
begin
  FreeLibrary(handle);
end;

{uses ActiveX;

Initialization
  CoInitialize(nil);
finalization
  CoUnInitialize;}

end.
 