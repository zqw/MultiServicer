unit WinService;

interface

uses
Windows,WinSvc,WinSvcEx,Forms,SysUtils;

function InstallService(const strServiceName,strDisplayName,strDescription,strFilename: string):Boolean;
//eg:InstallService('服务名称','显示名称','描述信息','服务文件');
function UninstallService(strServiceName:string):boolean;
function IsServiceInstalled(strServiceName:string):boolean;
function MyStopService(strServiceName:string):boolean;
function MyStartService(strServiceName:string):boolean;
function IsServiceStarted(strServiceName:string):boolean;

implementation

function StrLCopy(Dest: PChar; const Source: PChar; MaxLen: Cardinal): PChar; assembler;
asm
  PUSH EDI
  PUSH ESI
  PUSH EBX
  MOV ESI,EAX
  MOV EDI,EDX
  MOV EBX,ECX
  XOR AL,AL
  TEST ECX,ECX
  JZ @@1
  REPNE SCASB
  JNE @@1
  INC ECX
  @@1: SUB EBX,ECX
  MOV EDI,ESI
  MOV ESI,EDX
  MOV EDX,EDI
  MOV ECX,EBX
  SHR ECX,2
  REP MOVSD
  MOV ECX,EBX
  AND ECX,3
  REP MOVSB
  STOSB
  MOV EAX,EDX
  POP EBX
  POP ESI
  POP EDI
end;

function StrPCopy(Dest: PChar; const Source: string): PChar;
begin
  Result := StrLCopy(Dest, PChar(Source), Length(Source));
end;

function InstallService(const strServiceName,strDisplayName,strDescription,strFilename: string):Boolean;
var
  //ss : TServiceStatus;
  //psTemp : PChar;
  hSCM,hSCS:THandle;
  srvdesc : PServiceDescription;
  desc : string;
  //SrvType : DWord;
  lpServiceArgVectors:pchar;
begin
  Result:=False;
  //psTemp := nil;
  //SrvType := SERVICE_WIN32_OWN_PROCESS and SERVICE_INTERACTIVE_PROCESS;
  hSCM:=OpenSCManager(nil,nil,SC_MANAGER_ALL_ACCESS);//连接服务数据库
  if hSCM=0 then Exit;//MessageBox(hHandle,Pchar(SysErrorMessage(GetLastError)),'服务程序管理器',MB_ICONERROR+MB_TOPMOST);

  hSCS:=CreateService( //创建服务函数
    hSCM, // 服务控制管理句柄
    Pchar(strServiceName), // 服务名称
    Pchar(strDisplayName), // 显示的服务名称
    SERVICE_ALL_ACCESS, // 存取权利
    SERVICE_WIN32_OWN_PROCESS or SERVICE_INTERACTIVE_PROCESS,// 服务类型 SERVICE_WIN32_SHARE_PROCESS
    SERVICE_AUTO_START, // 启动类型
    SERVICE_ERROR_IGNORE, // 错误控制类型
    Pchar(strFilename), // 服务程序
    nil, // 组服务名称
    nil, // 组标识
    nil, // 依赖的服务
    nil, // 启动服务帐号
    nil); // 启动服务口令
  if hSCS>0 then
  begin
    if Assigned(ChangeServiceConfig2) then
    begin
      desc := Copy(strDescription,1,1024);
      GetMem(srvdesc,SizeOf(TServiceDescription));
      GetMem(srvdesc^.lpDescription,Length(desc) + 1);
      try
        StrPCopy(srvdesc^.lpDescription, desc);
        ChangeServiceConfig2(hSCS,SERVICE_CONFIG_DESCRIPTION,srvdesc);
      finally
        FreeMem(srvdesc^.lpDescription);
        FreeMem(srvdesc);
      end;
    end;
    lpServiceArgVectors := nil;
    Result:=True;
    if StartService(hSCS, 0, lpServiceArgVectors) then //启动服务
    begin
      Result:=True;
    end
    else
      Application.MessageBox(Pchar(SysErrorMessage(GetLastError)),Pchar(Application.Title));
    CloseServiceHandle(hSCS); //关闭句柄
  end
  else
    Application.MessageBox(Pchar(SysErrorMessage(GetLastError)),Pchar(Application.Title));
  CloseServiceHandle(hSCM);
end;

function UninstallService(strServiceName:string):boolean;
var
  SCManager: SC_HANDLE;
  Service: SC_HANDLE;
  Status: TServiceStatus;
begin
  Result:=false;
  SCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCManager>0 then
  begin
    Service := OpenService(SCManager, Pchar(strServiceName), SERVICE_ALL_ACCESS);
    if Service>0 then
    begin
      if QueryServiceStatus(Service,Status)=true then
      begin
        if Status.dwCurrentState<>SERVICE_STOPPED then
        begin
          if ControlService(Service, SERVICE_CONTROL_STOP, Status)=true then
            Result:=DeleteService(Service);
        end
        else
          Result:=DeleteService(Service);
      end;
      CloseServiceHandle(Service);
    end;
    CloseServiceHandle(SCManager);
  end;
end;

function MyStartService(strServiceName:string):boolean;
var
  SCManager: SC_HANDLE;
  Service: SC_HANDLE;
  //Status: TServiceStatus;
  lpServiceArgVectors:pchar;
begin
  Result:=false;
  SCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCManager>0 then
  begin
    Service := OpenService(SCManager, Pchar(strServiceName), SERVICE_ALL_ACCESS);
    if Service>0 then
    begin
      Result:=StartService(Service, 0, lpServiceArgVectors);//启动服务
      CloseServiceHandle(Service);
    end;
    CloseServiceHandle(SCManager);
  end;
end;

function MyStopService(strServiceName:string):boolean;
var
  SCManager: SC_HANDLE;
  Service: SC_HANDLE;
  Status: TServiceStatus;
begin
  Result:=false;
  SCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCManager>0 then
  begin
    Service := OpenService(SCManager, Pchar(strServiceName), SERVICE_ALL_ACCESS);
    if Service>0 then
    begin
      Result:=ControlService(Service, SERVICE_CONTROL_STOP, Status);
      CloseServiceHandle(Service);
    end;
    CloseServiceHandle(SCManager);
  end;
end;

function IsServiceInstalled(strServiceName:string):boolean;
var
  SCManager: SC_HANDLE;
  Service: SC_HANDLE;
  //Status: TServiceStatus;
begin
  Result:=false;
  SCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCManager>0 then
  begin
    Service := OpenService(SCManager, Pchar(strServiceName), SERVICE_ALL_ACCESS);
    if Service>0 then
    begin
      Result:=true;
      CloseServiceHandle(Service);
    end;
    CloseServiceHandle(SCManager);
  end;
end;

function IsServiceStarted(strServiceName:string):boolean;
var
  SCManager: SC_HANDLE;
  Service: SC_HANDLE;
  Status: TServiceStatus;
begin
  Result:=false;
  SCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if SCManager>0 then
  begin
    Service := OpenService(SCManager, Pchar(strServiceName), SERVICE_ALL_ACCESS);
    if Service>0 then
    begin
      if QueryServiceStatus(Service,Status)=true then
        if Status.dwCurrentState =SERVICE_RUNNING then
          Result :=True;
      CloseServiceHandle(Service);
    end;
    CloseServiceHandle(SCManager);
  end;
end;
end.
 
