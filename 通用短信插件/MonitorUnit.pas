//v1.1版，为适应南昌电务段短信通知，增加了自定义发送的SQL语句，增加了发送日志
//v1.2版，增加了转发收到的短信功能
unit MonitorUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus,
  ToolWin, Buttons,IniFiles,StrUtils, DB,
  ADODB, ActnList, ImgList,ShellAPI;

type
  TMonitorForm = class(TForm)
    Timer1: TTimer;
    Memo1: TMemo;
    qryReceive: TADOQuery;
    qrySend: TADOQuery;
    ADOConnection1: TADOConnection;
    qrySendLog: TADOQuery;
    CoolBar1: TCoolBar;
    StatusBar: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ActionList1: TActionList;
    StartAction: TAction;
    StopAction: TAction;
    TestAction: TAction;
    SettingAction: TAction;
    HelpAction: TAction;
    AboutAction: TAction;
    ExitAction: TAction;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    DataImageList: TImageList;
    StatisticalAction: TAction;
    ToolButton3: TToolButton;
    ToolButton12: TToolButton;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    N5: TMenuItem;
    ToolButton6: TToolButton;
    PluginsAction: TAction;
    procedure Timer1Timer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure TestActionExecute(Sender: TObject);
    procedure SettingActionExecute(Sender: TObject);
    procedure StartActionExecute(Sender: TObject);
    procedure StopActionExecute(Sender: TObject);
    procedure StatisticalActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure UpdateSendRecord(id,state:integer);
    procedure DoReceived(Source,Content:string);
  public
    { Public declarations }
    ComPort:integer;
    ComBaud:integer;
    Interval:integer;//发送间隔
    PhoneNumber:string;//本机号码
    IsRelay:integer;//是否转发
    RelayNumber:string;//转发号码
    ServiceName:string;//服务名称
    ServiceMemo:string;//服务描述
    SendOKCount:integer;
    SendFailCount:integer;
    ReceiveCount:integer;
    //function Authorize():boolean;
    procedure MemoMessage(str:string);
  end;

var
  MonitorForm: TMonitorForm;
  smshandle:THandle;
  function GetProductVersion():string;


implementation

uses WefeelSMSDLL,StatisticalUnit, MyAbout, SettingUnit, TestSendUnit;//,DAMPlusAppApis;

{$R *.dfm}

//统一格式显示信息
procedure TMonitorForm.MemoMessage(str:string);
begin
  Memo1.Lines.BeginUpdate;
  if Memo1.Lines.Count>300 then
    Memo1.Lines.Delete(0);
  Memo1.Lines.EndUpdate;
  Memo1.Lines.Add(DateTimeToStr(Now())+'|'+str);
  //Memo1.Refresh;
end;

//从Project信息中取版本号并显示在标题
function GetProductVersion():string;
const
  InfoNum = 10;
  InfoStr: array[1..InfoNum] of string = ('CompanyName', 'FileDescription', 'FileVersion', 'InternalName', 'LegalCopyright', 'LegalTradeMarks', 'OriginalFileName', 'ProductName', 'ProductVersion', 'Comments');
var
  S: string;
  n, Len: DWORD;
  Buf: PChar;
  Value: PChar;
begin
  //取系统版本
  S := GetModuleName(HInstance);
  n := GetFileVersionInfoSize(PChar(S), n);
  if n > 0 then
  begin
    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(S), 0, n, Buf);
    if VerQueryValue(Buf, PChar('StringFileInfo\080403A8\' + InfoStr[3]), Pointer(Value), Len) then
    begin
        Result:=Value;
    end;
    FreeMem(Buf, n);
  end;
end;

procedure TMonitorForm.Timer1Timer(Sender: TObject);
var
  strCode, strTime, strContent: array[0..500] of char;
  Source,Dest,MessageTime,MessageContent:string;
  id,state:integer;
  MessageSender:Variant;
begin
  Timer1.Enabled:=false;

  try

    if true then
    begin
      //检查是否收到短信
      if SMSGetSMS(@strCode, @strTime, @strContent) then
      begin
        //整理数据
        Source:=Trim(strCode);
        if Length(Source)>11 then
        begin
          if Pos('+86',Source)=1 then
            Delete(Source,1,3);
          if Pos('86',Source)=1 then
            Delete(Source,1,2);
        end;
        Dest:='';
        MessageTime:=Trim(strTime);
        MessageContent:=Trim(strContent);

        Inc(ReceiveCount);
        MemoMessage('接收|'+Source+'|'+MessageContent);

        qryReceive.Open;
        if qryReceive.Active then
        begin
          qryReceive.Append;
          qryReceive.FieldByName('号码').Value:=Source;
          qryReceive.FieldByName('时间').Value:=MessageTime;
          qryReceive.FieldByName('内容').Value:=MessageContent;
          qryReceive.FieldByName('被叫').Value:=PhoneNumber;
          qryReceive.Post;
        end;
        qryReceive.Close;
        DoReceived(Source,MessageContent);
      end;
    end;
    //发送短信
    if SMSQueueLength()<10 then
    begin //防止发送队列太多
      qrySend.Close;
      qrySend.Open;
      if qrySend.RecordCount>0 then
      begin
        qrySend.First;
        Dest:=Trim(qrySend.FieldByName('号码').AsString);
        MessageContent:=Trim(qrySend.FieldByName('内容').AsString);
        MessageSender:=qrySend.FieldValues['来源'];
        qrySend.Delete;
        qrySendLog.Close;
        qrySendLog.Open;
        qrySendLog.Append;
        qrySendLog.FieldByName('号码').AsString:=Dest;
        qrySendLog.FieldByName('内容').AsString:=MessageContent;
        qrySendLog.FieldValues['来源']:=MessageSender;
        qrySendLog.FieldByName('结果').AsInteger:=0; //发送结果未知
        qrySendLog.FieldByName('主叫').AsString:=PhoneNumber;
        qrySendLog.Post;
        id:=qrySendLog.FieldByName('ID').AsInteger;
        qrySendLog.Close;
        SMSPutSMS(PChar(Dest),PChar(MessageContent),id);
        MemoMessage('发送|'+Dest+'|'+MessageContent+'|'+IntToStr(id));
      end;
      qrySend.Close;
    end;
    //检查发送结果
    if SMSGetState(id,state) then
    begin
      if state=0 then //发送成功
      begin
        UpdateSendRecord(id,1);
        Inc(SendOKCount);
        MemoMessage('提示|'+IntToStr(id)+'|发送成功');
      end
      else //发送失败
      begin
        UpdateSendRecord(id,-1);
        Inc(SendFailCount);
        MemoMessage('提示|'+IntToStr(id)+'|发送失败');
      end;
    end;
  except
    on e:Exception do
    begin
      MemoMessage(e.Message);
      ADOConnection1.Close;
    end;
  end;

  //刷新发送队列
  StatusBar.Panels[1].Text := '发送队列：'+IntToStr(SMSQueueLength()) + '条';
  StatusBar.Panels[2].Text := '发送成功：'+IntToStr(SendOKCount) + '条'
    +' | 发送失败：'+IntToStr(SendFailCount) + '条'
    +' | 接收：'+IntToStr(ReceiveCount) + '条';
  Timer1.Enabled:=true;
end;

procedure TMonitorForm.FormDestroy(Sender: TObject);
begin
  StopActionExecute(nil);
  UnloadSMSDll(smshandle);
end;

procedure TMonitorForm.UpdateSendRecord(id,state:integer);
var
  qry:TADOQuery;
begin
  qry:=TADOQuery.Create(nil);
  qry.Connection:=ADOConnection1;
  qry.SQL.Text:='UPDATE td发送日志 SET 结果='+IntToStr(state)+' WHERE id='+IntToStr(id);
  try
    qry.ExecSQL;
  except
  end;
  qry.Free;
end;

procedure TMonitorForm.ExitActionExecute(Sender: TObject);
begin
  Close();
end;

procedure TMonitorForm.TestActionExecute(Sender: TObject);
begin
  if not Assigned(TestSendForm) then
    TestSendForm:=TTestSendForm.Create(Application);
  TestSendForm.ShowModal();
end;

procedure TMonitorForm.SettingActionExecute(Sender: TObject);
begin
  if not Assigned(SettingForm) then
    SettingForm:=TSettingForm.Create(Application);
  if SettingForm.ShowModal()=mrOK then
  begin
    if Self.StopAction.Enabled=true then
    begin
      Self.StopActionExecute(nil);
      Self.StartActionExecute(nil);
    end;
  end;
end;

procedure TMonitorForm.StartActionExecute(Sender: TObject);
var
  ini: TIniFile;
  qry:TADOQuery;
begin
  //读配置文件
  //ini := TIniFile.Create(GetCurrentDir+'\'+'SMSPlugin.INI');
  ini := TIniFile.Create(ChangeFileExt(GetModuleName(HInstance),'.INI'));
  Self.Caption := ini.ReadString('Setup', '程序名称', '短信网关');
  ADOConnection1.Close;
  ADOConnection1.ConnectionString := ini.ReadString('Setup', '数据库', 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=SMSBuffer.mdb;Persist Security Info=False');
  qrySend.SQL.Text:=ini.ReadString('Setup','SendSQL','SELECT TOP 1 * FROM td发送 ORDER BY ID DESC');
  ComPort:= ini.ReadInteger('SMS', 'ComPort', 1);
  ComBaud:= ini.ReadInteger('SMS', 'ComBaud', 9600);
  Interval:=ini.ReadInteger('SMS','Interval',7);
  PhoneNumber:=ini.ReadString('Setup','本机号码','');
  IsRelay:= ini.ReadInteger('Setup', '是否转发', 0);
  RelayNumber:= ini.ReadString('Setup', '转发号码', '');
  ini.Free;
  //程序标题
  Self.Caption:=Self.Caption+' '+GetProductVersion();

  try
    ADOConnection1.Open //启动与数据库服务器的连接
  except
    ShowMessage('与数据库服务器连接未成功，请检查INI文件配置');
    //Close;
  end;

  SetCurrentDir(ExtractFileDir(Application.ExeName));
  //建发送表
  qry:=TADOQuery.Create(nil);
  qry.Connection:=ADOConnection1;
  qry.SQL.Clear;
  qry.SQL.Add('CREATE TABLE [dbo].[td发送] (');
	qry.SQL.Add('	[ID] [int] IDENTITY (1, 1) NOT NULL ,');
	qry.SQL.Add('	[号码] [char] (20) COLLATE Chinese_PRC_CI_AS NULL ,');
	qry.SQL.Add('	[内容] [varchar] (500) COLLATE Chinese_PRC_CI_AS NULL ');
	qry.SQL.Add('	[来源] [varchar] (50) COLLATE Chinese_PRC_CI_AS NULL ');
  qry.SQL.Add(') ON [PRIMARY];');
  qry.SQL.Add('ALTER TABLE [dbo].[td发送] WITH NOCHECK ADD ');
  qry.SQL.Add('	CONSTRAINT [PK_td发送] PRIMARY KEY  CLUSTERED ');
  qry.SQL.Add('	([ID])  ON [PRIMARY];');
  try
    qry.ExecSQL;
  except
  end;
  //建接收表
  qry.SQL.Clear;
  qry.SQL.Add('CREATE TABLE [dbo].[td接收] (');
  qry.SQL.Add('	[ID] [int] IDENTITY (1, 1) NOT NULL ,');
  qry.SQL.Add('	[号码] [char] (20) COLLATE Chinese_PRC_CI_AS NULL ,');
  qry.SQL.Add('	[时间] [char] (20) COLLATE Chinese_PRC_CI_AS NULL ,');
  qry.SQL.Add('	[内容] [varchar] (500) COLLATE Chinese_PRC_CI_AS NULL ');
  qry.SQL.Add(') ON [PRIMARY];');
  qry.SQL.Add('ALTER TABLE [dbo].[td接收] WITH NOCHECK ADD ');
  qry.SQL.Add('	CONSTRAINT [PK_td接收] PRIMARY KEY  CLUSTERED ');
  qry.SQL.Add('	([ID])  ON [PRIMARY];');
  try
    qry.ExecSQL;
  except
  end;
  //建发送日志表
  qry.SQL.Clear;
  qry.SQL.Add('CREATE TABLE [dbo].[td发送日志] (');
	qry.SQL.Add('	[ID] [int] IDENTITY (1, 1) NOT NULL ,');
  qry.SQL.Add('	[时间] [datetime] NULL  ,');
	qry.SQL.Add('	[号码] [char] (20) COLLATE Chinese_PRC_CI_AS NULL ,');
	qry.SQL.Add('	[内容] [varchar] (500) COLLATE Chinese_PRC_CI_AS NULL ,');
	qry.SQL.Add('	[来源] [varchar] (50) COLLATE Chinese_PRC_CI_AS NULL ');
  qry.SQL.Add('	[结果] [int] NULL ');
  qry.SQL.Add(') ON [PRIMARY];');
  qry.SQL.Add('ALTER TABLE [dbo].[td发送日志] WITH NOCHECK ADD ');
  qry.SQL.Add(' CONSTRAINT [DF_td发送日志_时间] DEFAULT (getdate()) FOR [时间],');
  qry.SQL.Add('	CONSTRAINT [PK_td发送日志] PRIMARY KEY  CLUSTERED ');
  qry.SQL.Add('	([ID])  ON [PRIMARY];');
  try
    qry.ExecSQL;
    MemoMessage('提示|建库成功');
  except
  end;
  qry.SQL.Clear;
  qry.SQL.Text:='ALTER TABLE td发送日志 ADD [主叫] [VARCHAR](50) NOT NULL,CONSTRAINT [DF_td发送日志_主叫] DEFAULT ('''') FOR [主叫];'
      +' ALTER TABLE td接收 ADD [被叫] [VARCHAR](50) NOT NULL,CONSTRAINT [DF_td接收_被叫] DEFAULT ('''') FOR [被叫]';
  try
    qry.ExecSQL;
  except
  end;
  qry.Free;

  MemoMessage('提示|版本：'+Self.Caption+'|给本网关发TJ短信可查询统计信息');
  SMSCreate();
  SMSSetting(ComPort,ComBaud);
  try
    SMSStart();
    StatusBar.Panels[0].Text := '连接短信设备成功';
    MemoMessage('提示|打开串口成功');
    Timer1.Enabled:=true;
    StartAction.Enabled:=false;
    StopAction.Enabled:=true;
  except
    StatusBar.Panels[0].Text := '连接短信设备失败';
    MemoMessage('提示|打开串口失败');
    Timer1.Enabled:=false;
    StartAction.Enabled:=True;
    StopAction.Enabled:=false;
  end;
  //Authorize(); //控制开启Timer1

  Timer1.Enabled:=true;
end;

procedure TMonitorForm.StopActionExecute(Sender: TObject);
begin
  SMSStop();
  SMSFree();
  Timer1.Enabled:=false;
  StartAction.Enabled:=true;
  StopAction.Enabled:=false;
end;

procedure TMonitorForm.StatisticalActionExecute(Sender: TObject);
begin
  if not Assigned(StatisticalForm) then
    StatisticalForm:=TStatisticalForm.Create(Application);
  StatisticalForm.ShowModal;
end;

procedure TMonitorForm.DoReceived(Source,Content:string);
var
  rq:string;
  substr:string;
  qry:TADOQuery;
  mess:string;
  hint:string;
begin
  hint:='查询统计信息短信格式：TJ加年月或年月日。如：TJ200706或TJ20070601';
  if Pos('TJ',UpperCase(Content))=1 then
  begin
    qry:=TADOQuery.Create(nil);
    qry.Connection:=ADOConnection1;
    rq:=Trim(Copy(Content,3,Length(Content)-2));
    if Length(rq)=6 then //按月
    begin
      mess:=rq+'统计结果:';
      if Pos('JET.OLEDB',UpperCase(ADOConnection1.Provider))>0 then  //access
        substr:='FORMAT(时间,''YYYYMM'')'
      else                             //microsoft sql server
        substr:='CONVERT(CHAR(6),时间,112)';
      qry.SQL.Text:='SELECT COUNT(*) AS 数量 FROM td发送日志 WHERE 结果>0 AND 主叫='+QuotedStr(PhoneNumber)+' AND '+substr+'='+QuotedStr(rq);
      try
        qry.Open;
        if not qry.Eof then mess:=mess+'发送'+qry.Fields[0].AsString+'条';
        qry.Close;
        qry.SQL.Text:='SELECT COUNT(*) AS 数量 FROM td接收 WHERE 被叫='+QuotedStr(PhoneNumber)+' AND '+substr+'='+QuotedStr(rq);
        qry.Open;
        if not qry.Eof then mess:=mess+' 接收'+qry.Fields[0].AsString+'条';
        qry.Close;
      except
        mess:='查询错误！'+hint;
      end;
    end
    else if Length(rq)=8 then //按日
    begin
      mess:=rq+'统计结果:';
      if Pos('JET.OLEDB',UpperCase(ADOConnection1.Provider))>0 then  //access
        substr:='FORMAT(时间,''YYYYMMDD'')'
      else                            //microsoft sql server
        substr:='CONVERT(CHAR(8),时间,112)';
      try
        qry.SQL.Text:='SELECT COUNT(*) AS 数量 FROM td发送日志 WHERE 结果>0 AND 主叫='+QuotedStr(PhoneNumber)+' AND '+substr+'='+QuotedStr(rq);
        qry.Open;
        if not qry.Eof then mess:=mess+'发送'+qry.Fields[0].AsString+'条';
        qry.Close;
        qry.SQL.Text:='SELECT COUNT(*) AS 数量 FROM td接收 WHERE 被叫='+QuotedStr(PhoneNumber)+' AND '+substr+'='+QuotedStr(rq);
        qry.Open;
        if not qry.Eof then mess:=mess+' 接收'+qry.Fields[0].AsString+'条';
        qry.Close;
      except
        mess:='查询错误！'+hint;
      end;
    end
    else
    begin
      mess:=hint;
    end;
    qry.Free;
    qrySend.Open;
    qrySend.Append;
    qrySend.FieldByName('号码').Value:=Source;
    qrySend.FieldByName('内容').Value:=mess;
    qrySend.FieldByName('来源').Value:='统计';
    qrySend.Post;
    qrySend.Close;
  end
  else //转发信息到用户指定的手机号
  begin
    if( (IsRelay<>0) and (RelayNumber<>'') ) then
    begin
      qrySend.Open;
      qrySend.Append;
      qrySend.FieldByName('号码').Value:=RelayNumber;
      qrySend.FieldByName('内容').Value:='('+Source+')'+Content;
      qrySend.FieldByName('来源').Value:='转发';
      qrySend.Post;
      qrySend.Close;
    end;
  end;
end;

procedure TMonitorForm.AboutActionExecute(Sender: TObject);
begin
  if not Assigned(MyAboutBox) then
    MyAboutBox:=TMyAboutBox.Create(Application);
  MyAboutBox.ShowModal;
end;

{function TMonitorForm.Authorize():boolean;
var
  ret:integer;
  Buffer: array[0..16] of char;
begin
  Result:=true;
  ret:=DAMPlusAppApis.DOGFIND(0003);                   //查找指定应用程序标识的加密锁
 	if (ret = 0) then
  begin
    MemoMessage('请插软件狗');
    Timer1.Enabled:=false;
    Exit;
  end;

  ret := DAMPlusAppApis.DOGOPEN($1D26,$D5AF,$77A2,$0000);
 	if (ret <> 0) then
  begin
    MemoMessage('请插正确的软件狗');
    Timer1.Enabled:=false;
    Exit;
  end;

  ret := DAMPlusAppApis.DOGCHECK($7777);
  if ret <> 0 then
  begin
    MemoMessage('密码错误');
    Timer1.Enabled:=false;
    DAMPlusAppApis.DOGCLOSE;
    Exit;
  end;
  ret := DAMPlusAppApis.DOGREADBUFFER(0 , 16 , @Buffer);
  if ret <> 0 then
  begin
    MemoMessage('读狗错误');
    Timer1.Enabled:=false;
    DAMPlusAppApis.DOGCLOSE;
    Exit;
  end
  else
  begin
    Timer1.Enabled:=true;
  end;
  DAMPlusAppApis.DOGCLOSE;
end;}

procedure TMonitorForm.FormCreate(Sender: TObject);
begin
  smshandle:=LoadSMSDll();
  StartActionExecute(nil);
end;


procedure TMonitorForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caHide;
end;

{initialization
  CoInitialize(nil);
finalization
  CoUnInitialize;}

end.
