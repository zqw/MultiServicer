unit InstallUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TInstallForm = class(TForm)
    Label1: TLabel;
    btnInstall: TBitBtn;
    btnUninstall: TBitBtn;
    BitBtn1: TBitBtn;
    btnStop: TBitBtn;
    btnStart: TBitBtn;
    btnSetup: TBitBtn;
    btnAbout: TBitBtn;
    procedure btnInstallClick(Sender: TObject);
    procedure Refresh;
    procedure btnUninstallClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InstallForm: TInstallForm;

implementation

uses WinService, MyAbout, SettingUnit;

{$R *.dfm}

procedure TInstallform.Refresh;
begin
  if IsServiceInstalled('SMSService') then
  begin
    btnInstall.Enabled:=false;
    btnUninstall.Enabled:=true;
    if IsServiceStarted('SMSService') then
    begin
      btnStart.Enabled:=false;
      btnStop.Enabled:=true;
    end
    else
    begin
      btnStart.Enabled:=true;
      btnStop.Enabled:=false;
    end;
  end
  else
  begin
    btnInstall.Enabled:=true;
    btnUninstall.Enabled:=false;
    btnStart.Enabled:=false;
    btnStop.Enabled:=false;
  end;
end;

procedure TInstallForm.btnInstallClick(Sender: TObject);
begin
  if InstallService('SMSService','短信网关服务器','江西闻风科技有限公司',Application.ExeName+' /service') then
    Application.MessageBox('安装成功。','提示')
  else
    Application.MessageBox('安装失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.btnUninstallClick(Sender: TObject);
begin
  if UninstallService('SMSService') then
    Application.MessageBox('卸载成功。','提示')
  else
    Application.MessageBox('卸载失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.btnStartClick(Sender: TObject);
begin
  if MyStartService('SMSService') then
    Application.MessageBox('启动成功。','提示')
  else
    Application.MessageBox('启动失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.btnStopClick(Sender: TObject);
begin
  if MyStopService('SMSService') then
    Application.MessageBox('停止成功。','提示')
  else
    Application.MessageBox('停止失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.FormShow(Sender: TObject);
begin
  Self.Refresh;
end;

procedure TInstallForm.btnAboutClick(Sender: TObject);
begin
  if not Assigned(MyAboutBox) then
    MyAboutBox:=TMyAboutBox.Create(Application);
  MyAboutBox.ShowModal;
end;

procedure TInstallForm.btnSetupClick(Sender: TObject);
begin
  if not Assigned(SettingForm) then
    SettingForm:=TSettingForm.Create(Application);
  if SettingForm.ShowModal()=mrOK then
  begin
    ShowMessage('重启服务后参数才能生效！');
  end;
end;

procedure TInstallForm.BitBtn1Click(Sender: TObject);
begin
  Self.Close();
end;

end.
