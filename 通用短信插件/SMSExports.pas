unit SMSExports;

interface

uses Controls,Forms,ActiveX;

  function Start():boolean;stdcall; //开始运行插件，必须实现才能被主程序通过ini文件调用
  function Stop():boolean;stdcall;  //停止运行插件，可选
  function Setup():boolean;stdcall; //进行插件设置，可选
  function About():boolean;stdcall; //显示插件版本信息，可选
  function IsRun():boolean;stdcall; //返回插件是否正在运行，可选
  function Show():boolean;stdcall;  //显示插件主窗口，可选

implementation
uses MonitorUnit, SettingUnit, MyAbout;

function Start(): boolean; stdcall;
begin
  MonitorForm:=TMonitorForm.Create(Application);
  Result:=True;
end;

function Stop(): boolean; stdcall;
begin
  MonitorForm.Close;
  MonitorForm.Free;
  MonitorForm:=nil;
  Result:=true;
end;

function Setup():boolean;stdcall;
begin
  if not Assigned(SettingForm) then
    SettingForm:=TSettingForm.Create(Application);
  if SettingForm.ShowModal()=mrOK then
  begin
    if Assigned(MonitorForm) then
    begin
      if MonitorForm.StopAction.Enabled=true then
      begin
        MonitorForm.StopActionExecute(nil);
        MonitorForm.StartActionExecute(nil);
      end;
    end;
  end;
  Result:=true;
end;

function About():boolean;stdcall;
begin
  if not Assigned(MyAboutBox) then
    MyAboutBox:=TMyAboutBox.Create(Application);
  MyAboutBox.ShowModal;
  Result:=true;
end;

function IsRun():boolean;stdcall;
begin
  Result:=false;
  if Assigned(MonitorForm) then Result:=true;
end;

function Show():boolean;stdcall;
begin
  if Assigned(MonitorForm) then
  begin
    MonitorForm.Show;
    Result:=true;
    Exit;
  end;
  Application.MessageBox('请运行插件','错误',0);
  Result:=True;
end;


end.
