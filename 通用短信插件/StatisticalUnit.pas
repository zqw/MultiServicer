unit StatisticalUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, ExtCtrls, ToolWin, ComCtrls;

type
  TStatisticalForm = class(TForm)
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    CoolBar1: TCoolBar;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StatisticalForm: TStatisticalForm;

implementation

uses MonitorUnit;

{$R *.dfm}

procedure TStatisticalForm.FormShow(Sender: TObject);
begin
  RadioGroup1Click(nil);
end;

procedure TStatisticalForm.RadioGroup1Click(Sender: TObject);
var
  substr:string;
begin
  if Pos('JET.OLEDB',UpperCase(ADOQuery1.Connection.Provider))>0 then  //access
  begin
    if RadioGroup1.ItemIndex=0 then  //按月
      substr:='FORMAT(时间,''YYYYMM'')'
    else                             //按日
      substr:='FORMAT(时间,''YYYYMMDD'')';
  end
  else  //microsoft sql server
  begin
    if RadioGroup1.ItemIndex=0 then //按月
      substr:='CONVERT(CHAR(6),CAST(时间 AS DATETIME),112)'
    else                            //按日
      substr:='CONVERT(CHAR(8),CAST(时间 AS DATETIME),112)';
  end;
  ADOQuery1.Close;
  {ADOQuery1.SQL.Text:='SELECT ''发送'' AS 名称,'+substr+' AS 时间,COUNT(*) AS 数量'
      +' FROM td发送日志'
      +' WHERE 结果>0'
      +' GROUP BY '+substr
      +' UNION'
      +' SELECT ''接收'' AS 名称,'+substr+' AS 时间,COUNT(*) AS 数量'
      +' FROM td接收'
      +' GROUP BY '+substr;}
  ADOQuery1.SQL.Text:='SELECT a.时间,b.发送数量,c.接收数量 FROM'
      +' ((SELECT DISTINCT x.* FROM'
      +' (SELECT '+substr+' AS 时间'
      +' FROM td发送日志'
      +' WHERE 结果>0 AND 主叫='+QuotedStr(MonitorForm.PhoneNumber)
      +' UNION'
      +' SELECT '+substr+' AS 时间'
      +' FROM td接收'
      +' WHERE 被叫='+QuotedStr(MonitorForm.PhoneNumber)
      +' ) x) a'
      +' LEFT JOIN'
      +' (SELECT '+substr+' AS 时间,COUNT(*) AS 发送数量 FROM td发送日志 WHERE 结果>0 AND 主叫='+QuotedStr(MonitorForm.PhoneNumber)
      +' GROUP BY '+substr+') b ON a.时间=b.时间)'
      +' LEFT JOIN'
      +' (SELECT '+substr+' AS 时间,COUNT(*) AS 接收数量 FROM td接收 WHERE 被叫='+QuotedStr(MonitorForm.PhoneNumber)
      +' GROUP BY '+substr+') c ON a.时间=c.时间';
  ADOQuery1.Open;
end;

procedure TStatisticalForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  StatisticalForm:=nil;
end;

end.
