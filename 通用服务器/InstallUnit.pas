unit InstallUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,IniFiles;

const
  ServiceName='WefeelService';

type
  TInstallForm = class(TForm)
    Label1: TLabel;
    btnInstall: TBitBtn;
    btnUninstall: TBitBtn;
    BitBtn1: TBitBtn;
    btnStop: TBitBtn;
    btnStart: TBitBtn;
    btnSetup: TBitBtn;
    btnAbout: TBitBtn;
    procedure btnInstallClick(Sender: TObject);
    procedure Refresh;
    procedure btnUninstallClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InstallForm: TInstallForm;

implementation

uses WinService, MyAbout, SettingUnit;

{$R *.dfm}

procedure TInstallform.Refresh;
begin
  if IsServiceInstalled(ServiceName) then
  begin
    btnInstall.Enabled:=false;
    btnUninstall.Enabled:=true;
    if IsServiceStarted(ServiceName) then
    begin
      btnStart.Enabled:=false;
      btnStop.Enabled:=true;
    end
    else
    begin
      btnStart.Enabled:=true;
      btnStop.Enabled:=false;
    end;
  end
  else
  begin
    btnInstall.Enabled:=true;
    btnUninstall.Enabled:=false;
    btnStart.Enabled:=false;
    btnStop.Enabled:=false;
  end;
end;

procedure TInstallForm.btnInstallClick(Sender: TObject);
var
  ini:TIniFile;
  Name,Describe:string;
begin
  //读配置文件
  ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  Name:= ini.ReadString('Setup', '程序名称', '通用服务器');
  Describe:= ini.ReadString('Setup', '服务描述', '即可作服务也可作普通程序的可开发插件的通用服务器');
  ini.Free;
  if InstallService(ServiceName,Name,Describe,Application.ExeName+' /service') then
    Application.MessageBox('安装成功。','提示')
  else
    Application.MessageBox('安装失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.btnUninstallClick(Sender: TObject);
begin
  if UninstallService(ServiceName) then
    Application.MessageBox('卸载成功。','提示')
  else
    Application.MessageBox('卸载失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.btnStartClick(Sender: TObject);
begin
  if MyStartService(ServiceName) then
    Application.MessageBox('启动成功。','提示')
  else
    Application.MessageBox('启动失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.btnStopClick(Sender: TObject);
begin
  if MyStopService(ServiceName) then
    Application.MessageBox('停止成功。','提示')
  else
    Application.MessageBox('停止失败！','提示');
  Self.Refresh;
end;

procedure TInstallForm.FormShow(Sender: TObject);
var
  ini:TIniFile;
begin
  //读配置文件
  ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  Self.Caption:=ini.ReadString('Setup', '程序名称', '通用服务器');
  ini.Free;
  Self.Caption:=Self.Caption+' 服务管理';
  Application.Title:=Self.Caption;
  Self.Refresh;
end;

procedure TInstallForm.btnAboutClick(Sender: TObject);
begin
  if not Assigned(MyAboutBox) then
    MyAboutBox:=TMyAboutBox.Create(Application);
  MyAboutBox.ShowModal;
end;

procedure TInstallForm.btnSetupClick(Sender: TObject);
begin
  if not Assigned(SettingForm) then
    SettingForm:=TSettingForm.Create(Application);
  if SettingForm.ShowModal()=mrOK then
  begin
    ShowMessage('重新安装服务后参数才能生效！');
  end;
end;

procedure TInstallForm.BitBtn1Click(Sender: TObject);
begin
  Self.Close();
end;

end.
