{
专门为服务器开发的符合规范的dll，由Plugins.dll配置
}
unit ServerPlugin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls,ADODB,IniFiles,ActiveX;

type
  //A类插件，由ini配置加载
  TPlugin = Class
    FileName:string;  //插件文件名
    Name:string;      //插件名称
    AutoRun:boolean;  //是否自动运行本插件
    Address:THandle; //存取加载后的DLL的句柄
    //定义接口函数
    Start:function():boolean;stdcall; //开始运行插件，必须实现才能被主程序通过ini文件调用
    Stop:function():boolean;stdcall;  //停止运行插件，可选
    Setup:function():boolean;stdcall; //进行插件设置，可选
    About:function():boolean;stdcall; //显示插件版本信息，可选
    IsRun:function():boolean;stdcall; //返回插件是否正在运行，可选
    Show:function():boolean;stdcall;  //显示插件主窗口，可选
  end;

  procedure InitPlugins(); //初始化所有插件，如是自动运行的插件则自动运行
  procedure FreePlugins; //停止和释放所有插件
  function LoadPlugin(filename:string):TPlugin; //加载一个插件
  function StartPlugin(plugin:TPlugin):boolean; //运行一个插件
  function StopPlugin(plugin:TPlugin):boolean;  //停止一个插件
  function ShowPlugin(plugin:TPlugin):boolean;  //显示一个插件


var
  PluginList:TList;
  Plugins: TList; //指针列表,存放每一个DLL加载后的相关信息

implementation

uses MonitorUnit;

//装载插件
procedure InitPlugins();
var
  i: integer;
  Plugin: TPlugin; //存放插件信息的自定义的变量
  temp:string;
  name,filename:string;
  autorun:boolean;
  ini:TIniFile;
begin
  ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  PluginList:=TList.Create;
  for i:=1 to 20 do
  begin
    temp:='插件'+IntToStr(i);
    name:= ini.ReadString(temp, '名称', '');
    filename:= ini.ReadString(temp, '程序', '');
    if name='' then Break;
    if ini.ReadInteger(temp, '自动运行', 1)=0 then
      autorun:=false
    else
      autorun:=true;
    Plugin:=LoadPlugin(filename);
    if Plugin=nil then break;
    Plugin.Name:=name;
    Plugin.FileName:=filename;
    Plugin.AutoRun:=autorun;

    PluginList.Add(Plugin);

    if Plugin.AutoRun then
    begin
      StartPlugin(Plugin);
    end;

  end;
  ini.Free;
end;

function LoadPlugin(filename:string):TPlugin;
var
  Plugin: TPlugin; //存放插件信息的自定义的变量
begin
  Plugin:=TPlugin.Create;
  Plugin.Address := LoadLibrary(PChar(Extractfilepath(application.ExeName)+filename)); //装载DLL，返回句柄
  if Plugin.Address=0 then
  begin
    ShowMessage('加载'+Extractfilepath(application.ExeName)+filename+'失败!');
    Plugin.Free;
    Result:=nil;
    Exit;
  end;
  @Plugin.Start:=GetProcAddress(Plugin.Address,'Start'); //通过DLL句柄取得“获取插件标题”过程的入口地址
  @Plugin.Stop:=GetProcAddress(Plugin.Address,'Stop'); //通过DLL句柄取得“获取插件标题”过程的入口地址
  @Plugin.Setup:=GetProcAddress(Plugin.Address,'Setup'); //通过DLL句柄取得“获取插件标题”过程的入口地址
  @Plugin.About:=GetProcAddress(Plugin.Address,'About'); //通过DLL句柄取得“获取插件标题”过程的入口地址
  @Plugin.IsRun:=GetProcAddress(Plugin.Address,'IsRun'); //通过DLL句柄取得“获取插件标题”过程的入口地址
  @Plugin.Show:=GetProcAddress(Plugin.Address,'Show'); //通过DLL句柄取得“获取插件标题”过程的入口地址
  if @Plugin.Start=nil then
  begin
    ShowMessage('插件'+Extractfilepath(application.ExeName)+filename+'不是有效插件!');
    Plugin.Free;
    Result:=nil;
    Exit;
  end;
  Result:=Plugin;
end;

function StartPlugin(plugin:TPlugin):boolean;
begin
  Plugin.Start();
  Result:=true;
end;

function StopPlugin(plugin:TPlugin):boolean;
begin
  Plugin.Stop();
  Result:=true;
end;

function ShowPlugin(plugin:TPlugin):boolean;
begin
  Plugin.Show();
  Result:=true;
end;

procedure FreePlugins;
var
  i: integer;
begin
  if not Assigned(PluginList) then Exit;
  for i:=0 to PluginList.Count-1 do
  begin
    if TPlugin(PluginList[i]).IsRun then TPlugin(PluginList[i]).Stop;
    FreeLibrary(TPlugin(PluginList[i]).Address); //按DLL的句柄释放内存
  end;
  PluginList.free;
end;

end.
