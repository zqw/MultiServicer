unit ServiceUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  ActiveX,Forms;

type
  TWefeelService = class(TService)  //服务的类名必须和服务名一致 WefeelService
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  WefeelService: TWefeelService;

implementation

uses  MonitorUnit;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  WefeelService.Controller(CtrlCode);
end;

function TWefeelService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TWefeelService.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  while not Terminated do
  begin
    Sleep(30);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TWefeelService.ServiceExecute(Sender: TService);
begin
  while not Terminated do
  begin
    Sleep(30);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TWefeelService.ServicePause(Sender: TService; var Paused: Boolean);
begin
  Paused := True;
end;

procedure TWefeelService.ServiceShutdown(Sender: TService);
begin
  MonitorForm.Close;
  MonitorForm.Free;
  Status := csStopped;
  ReportStatus();
end;

procedure TWefeelService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  CoInitialize(nil);
  Svcmgr.Application.CreateForm(TMonitorForm, MonitorForm);
  MonitorForm.HelpAction.Visible:=false;
  MonitorForm.Hide;
  Started := True;
end;

procedure TWefeelService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  MonitorForm.Close;
  MonitorForm.Free;
  CoUnInitialize;
  Stopped := True;
end;

end.
