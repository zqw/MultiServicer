unit SettingUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, IniFiles;

type
  TSettingForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SettingForm: TSettingForm;

implementation

{$R *.dfm}

procedure TSettingForm.FormShow(Sender: TObject);
var
  ini: TIniFile;
begin
  ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  //读配置文件
  Edit1.Text := ini.ReadString('Setup', '程序名称', '通用服务器');
  Edit2.Text := ini.ReadString('Setup', '服务描述', '即可作服务也可作普通程序的可开发插件的通用服务器');
  ini.Free;
end;

procedure TSettingForm.BitBtn1Click(Sender: TObject);
var
  ini: TIniFile;
begin
  ini := TIniFile.Create(ChangeFileExt(Application.ExeName, '.INI'));
  ini.WriteString('Setup', '程序名称', Edit1.Text);
  ini.WriteString('Setup', '服务描述', Edit2.Text);
  ini.Free;

  ModalResult:=mrOK;
end;

procedure TSettingForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  SettingForm:=nil;
end;

procedure TSettingForm.BitBtn2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

end.

