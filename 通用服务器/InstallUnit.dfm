object InstallForm: TInstallForm
  Left = 366
  Top = 449
  Width = 337
  Height = 261
  Caption = #36890#29992#26381#21153#22120#32622
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 24
    Width = 297
    Height = 60
    Caption = 
      #35828#26126#65306#13'1'#12289#26356#25913#36873#39033#21518#65292#37325#26032#23433#35013#21551#21160#26381#21153#25165#29983#25928#12290#13'2'#12289'Windows NT/2000/XP/2003'#31995#32479#19979#65292#23433#35013#25110#21368#36733#26381#21153#24517#39035#25317 +
      #26377#31649#29702#21592#26435#38480
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object btnInstall: TBitBtn
    Left = 40
    Top = 128
    Width = 75
    Height = 25
    Caption = #23433#35013#26381#21153
    TabOrder = 0
    OnClick = btnInstallClick
  end
  object btnUninstall: TBitBtn
    Left = 40
    Top = 160
    Width = 75
    Height = 25
    Caption = #21368#36733#26381#21153
    TabOrder = 1
    OnClick = btnUninstallClick
  end
  object BitBtn1: TBitBtn
    Left = 40
    Top = 192
    Width = 249
    Height = 25
    Caption = #36864#20986
    TabOrder = 6
    OnClick = BitBtn1Click
  end
  object btnStop: TBitBtn
    Left = 128
    Top = 160
    Width = 75
    Height = 25
    Caption = #20572#27490#26381#21153
    TabOrder = 3
    OnClick = btnStopClick
  end
  object btnStart: TBitBtn
    Left = 128
    Top = 128
    Width = 75
    Height = 25
    Caption = #21551#21160#26381#21153
    TabOrder = 2
    OnClick = btnStartClick
  end
  object btnSetup: TBitBtn
    Left = 216
    Top = 128
    Width = 75
    Height = 25
    Caption = #35774#32622#21442#25968
    TabOrder = 4
    OnClick = btnSetupClick
  end
  object btnAbout: TBitBtn
    Left = 216
    Top = 160
    Width = 75
    Height = 25
    Caption = #20851#20110
    TabOrder = 5
    OnClick = btnAboutClick
  end
end
