
unit MonitorUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Menus,
  Buttons,IniFiles,StrUtils,  ActnList, ImgList,ShellAPI, RzTray,ServerPlugin,
  ToolWin;

const
  WM_TrayIcon = WM_USER + 1234;

type
  TMonitorForm = class(TForm)
    CoolBar1: TCoolBar;
    StatusBar: TStatusBar;
    ToolBar1: TToolBar;
    ActionList1: TActionList;
    StartPluginAction: TAction;
    StopPluginAction: TAction;
    HelpAction: TAction;
    AboutAction: TAction;
    ExitAction: TAction;
    DataImageList: TImageList;
    ListView1: TListView;
    MainMenu1: TMainMenu;
    N2: TMenuItem;
    F1: TMenuItem;
    N5: TMenuItem;
    N1: TMenuItem;
    N3: TMenuItem;
    mnuPlugins: TMenuItem;
    ShowPluginAction: TAction;
    N4: TMenuItem;
    N6: TMenuItem;
    ShowPluginAction1: TMenuItem;
    N7: TMenuItem;
    PopupMenu1: TPopupMenu;
    R1: TMenuItem;
    S1: TMenuItem;
    V1: TMenuItem;
    SetupPluginAction: TAction;
    AboutPluginAction: TAction;
    T1: TMenuItem;
    A1: TMenuItem;
    T2: TMenuItem;
    A2: TMenuItem;
    Timer1: TTimer;
    procedure FormDestroy(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ExitActionExecute(Sender: TObject);
    procedure HelpActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure StartPluginActionExecute(Sender: TObject);
    procedure StopPluginActionExecute(Sender: TObject);
    procedure mnuPluginsClick(Sender: TObject);
    procedure ShowPluginActionExecute(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure SetupPluginActionExecute(Sender: TObject);
    procedure AboutPluginActionExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    IconData: TNotifyIconData;
    MsgTaskbarRestart: Cardinal;
    procedure AddIconToTray;
    procedure DelIconFromTray;
    procedure TrayIconMessage(var Msg: TMessage); message WM_TrayIcon;
    procedure SysButtonMsg(var Msg: TMessage); message WM_SYSCOMMAND;
    procedure WndProc(var Msg: TMessage); override;
  public
    { Public declarations }
    function Authorize():boolean;
    procedure RefreshList;
    procedure RefreshItem(plugin:TPlugin);
  end;

var
  MonitorForm: TMonitorForm;
  function GetProductVersion():string;


implementation

uses MyAbout, SettingUnit, 
  WinService, ServiceUnit, AutoPlugin;

{$R *.dfm}

//从Project信息中取版本号并显示在标题
function GetProductVersion():string;
const
  InfoNum = 10;
  InfoStr: array[1..InfoNum] of string = ('CompanyName', 'FileDescription', 'FileVersion', 'InternalName', 'LegalCopyright', 'LegalTradeMarks', 'OriginalFileName', 'ProductName', 'ProductVersion', 'Comments');
var
  S: string;
  n, Len: DWORD;
  Buf: PChar;
  Value: PChar;
begin
  //取系统版本
  //S := Forms.Application.ExeName;
  S := GetModuleName(HInstance);
  n := GetFileVersionInfoSize(PChar(S), n);
  if n > 0 then
  begin
    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(S), 0, n, Buf);
    if VerQueryValue(Buf, PChar('StringFileInfo\080403A8\' + InfoStr[3]), Pointer(Value), Len) then
    begin
        Result:=Value;
    end;
    FreeMem(Buf, n);
  end;
end;

procedure TMonitorForm.FormDestroy(Sender: TObject);
begin
  //释放服务器插件
  ServerPlugin.FreePlugins();
  //释放自动插件
  AutoPlugin.FreePlugins;
  //消除TrayIcon
  //RzTrayIcon1.Free;
  DelIconFromTray;
end;

procedure TMonitorForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  //RzTrayIcon1.MinimizeApp;
end;

procedure TMonitorForm.ExitActionExecute(Sender: TObject);
begin
  Close();
end;

procedure TMonitorForm.HelpActionExecute(Sender: TObject);
begin
  ShellExecute(0,'open',PChar(ChangeFileExt(Application.ExeName,'.chm')),nil,nil,SW_SHOW);
end;

procedure TMonitorForm.AboutActionExecute(Sender: TObject);
begin
  if not Assigned(MyAboutBox) then
    MyAboutBox:=TMyAboutBox.Create(Application);
  MyAboutBox.ShowModal;
end;

function TMonitorForm.Authorize():boolean;
var
  ret:integer;
  Buffer: array[0..16] of char;
begin
  Result:=true;
  
end;

procedure TMonitorForm.FormCreate(Sender: TObject);
var
  ini:TIniFile;
begin
  //读配置文件
  ini := TIniFile.Create(ChangeFileExt(Forms.Application.ExeName, '.INI'));
  Self.Caption := ini.ReadString('Setup', '程序名称', '通用服务器');
  ini.Free;
  //程序标题
  Application.Title:=Self.Caption;
  Self.Caption:=Self.Caption+' '+GetProductVersion();
  //RzTrayIcon1.Hint:=Self.Caption;

  {$IF DEFINED(DOG)}
  if Authorize()=true then
  begin
    //加载自动插件
    ServerPlugin.InitPlugins();
  end;
  {$Message Hint '请注意：此次编译生成的程序需要软件狗！'}
  {$ELSE}
    ServerPlugin.InitPlugins();
  {$Message Hint '请注意：此次编译生成的程序不需要软件狗。'}
  {$IFEND}


  AutoPlugin.LoadPlugins(mnuPlugins);

  //FormStyle := fsStayOnTop; {窗口最前}
  SetWindowLong(Application.Handle, GWL_EXSTYLE, WS_EX_TOOLWINDOW); {不在任务栏显示}
  //注册explorer重建消息
  MsgTaskbarRestart := RegisterWindowMessage('TaskbarCreated');
  //初始化托盘图标的IconData
  ZeroMemory(@IconData, SizeOf(TNotifyIconData));
  IconData.cbSize := SizeOf(TNotifyIconData);
  IconData.Wnd := Handle;
  IconData.uID := 1;
  IconData.uFlags := NIF_MESSAGE or NIF_ICON or NIF_TIP;
  IconData.uCallbackMessage := WM_TrayIcon;
  IconData.hIcon := Application.Icon.Handle;
  IconData.szTip := '                                                          ';
  StrPCopy(IconData.szTip,Application.Title);
  Timer1.Enabled:=true;
end;


procedure TMonitorForm.FormShow(Sender: TObject);
begin
  RefreshList;
  ListView1.Selected:=ListView1.Items[0];
end;

procedure TMonitorForm.RefreshItem(plugin:TPlugin);
var
  item:TListItem;
begin
  item:=ListView1.FindData(0,plugin,true,true);
  if item<>nil then
  begin
    if TPlugin(item.Data).AutoRun=true then
      item.SubItems[2]:='自动'
    else
      item.SubItems[2]:='手动';
    if TPlugin(item.Data).IsRun()=true then
      item.SubItems[3]:='运行'
    else
      item.SubItems[3]:='停止';
  end;
  ListView1SelectItem(nil,item,true);
end;

procedure TMonitorForm.RefreshList;
var
  i:integer;
begin
  if not Assigned(PluginList) then Exit;
  ListView1.Clear;
  for i:=0 to PluginList.Count-1 do
  begin
    ListView1.AddItem(IntToStr(i+1),PluginList.Items[i]);
    ListView1.Items[i].SubItems.Add(TPlugin(PluginList.Items[i]).Name);
    ListView1.Items[i].SubItems.Add(TPlugin(PluginList.Items[i]).FileName);
    if TPlugin(PluginList.Items[i]).AutoRun then
      ListView1.Items[i].SubItems.Add('自动')
    else
      ListView1.Items[i].SubItems.Add('手动');
    if TPlugin(PluginList.Items[i]).IsRun()=true then
      ListView1.Items[i].SubItems.Add('运行')
    else
      ListView1.Items[i].SubItems.Add('停止');
  end;
end;

procedure TMonitorForm.ListView1SelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  if Selected then
  begin
    if TPlugin(Item.Data).IsRun then
    begin
      StartPluginAction.Enabled:=false;
      StopPluginAction.Enabled:=true;
      if @TPlugin(Item.Data).Stop=nil then StopPluginAction.Enabled:=false;
    end
    else
    begin
      StartPluginAction.Enabled:=true;
      StopPluginAction.Enabled:=false;
    end;
    if @TPlugin(Item.Data).Setup=nil then
      SetupPluginAction.Enabled:=false
    else
      SetupPluginAction.Enabled:=true;
    if @TPlugin(Item.Data).About=nil then
      AboutPluginAction.Enabled:=false
    else
      AboutPluginAction.Enabled:=true;
    if @TPlugin(Item.Data).Show=nil then
      ShowPluginAction.Enabled:=false
    else
      ShowPluginAction.Enabled:=true;
  end;
end;

procedure TMonitorForm.StartPluginActionExecute(Sender: TObject);
begin
  if (ListView1.ItemFocused<>nil) and (ListView1.Selected<>ListView1.ItemFocused) then
    ListView1.Selected:=ListView1.ItemFocused;
  if ListView1.Selected=nil then Exit;
  ServerPlugin.StartPlugin(TPlugin(ListView1.Selected.Data));
  RefreshItem(ListView1.Selected.Data);
end;

procedure TMonitorForm.StopPluginActionExecute(Sender: TObject);
begin
  if (ListView1.ItemFocused<>nil) and (ListView1.Selected<>ListView1.ItemFocused) then
    ListView1.Selected:=ListView1.ItemFocused;
  if ListView1.Selected=nil then Exit;
  ServerPlugin.StopPlugin(TPlugin(ListView1.Selected.Data));
  RefreshItem(ListView1.Selected.Data);
end;

procedure TMonitorForm.ShowPluginActionExecute(Sender: TObject);
begin
  if (ListView1.ItemFocused<>nil) and (ListView1.Selected<>ListView1.ItemFocused) then
    ListView1.Selected:=ListView1.ItemFocused;
  if ListView1.Selected=nil then Exit;
  ServerPlugin.ShowPlugin(TPlugin(ListView1.Selected.Data));
  RefreshItem(ListView1.Selected.Data);
end;

procedure TMonitorForm.SetupPluginActionExecute(Sender: TObject);
begin
  if (ListView1.ItemFocused<>nil) and (ListView1.Selected<>ListView1.ItemFocused) then
    ListView1.Selected:=ListView1.ItemFocused;
  if ListView1.Selected=nil then Exit;
  TPlugin(ListView1.Selected.Data).Setup();
end;

procedure TMonitorForm.AboutPluginActionExecute(Sender: TObject);
begin
  if (ListView1.ItemFocused<>nil) and (ListView1.Selected<>ListView1.ItemFocused) then
    ListView1.Selected:=ListView1.ItemFocused;
  if ListView1.Selected=nil then Exit;
  TPlugin(ListView1.Selected.Data).About();
end;

procedure TMonitorForm.ListView1DblClick(Sender: TObject);
begin
  ShowPluginActionExecute(nil);
end;

procedure TMonitorForm.mnuPluginsClick(Sender: TObject);
begin
  //触发自动插件
  ClickPlugins(Sender);
end;

procedure TMonitorForm.AddIconToTray;
begin

  Shell_NotifyIcon(NIM_ADD, @IconData);

  //if not Shell_NotifyIcon(NIM_ADD, @IconData) then
    //Timer1.Enabled:=false;
end;

procedure TMonitorForm.DelIconFromTray;
begin
  Shell_NotifyIcon(NIM_DELETE, @IconData);
end;

procedure TMonitorForm.SysButtonMsg(var Msg: TMessage);
begin
  if (Msg.wParam = SC_CLOSE) or (Msg.wParam = SC_MINIMIZE) then Hide
  else inherited; // 执行默认动作
end;

procedure TMonitorForm.TrayIconMessage(var Msg: TMessage);
begin
  if (Msg.LParam = WM_LBUTTONDBLCLK) then
  begin
    Show();
    BringToFront;
  end;
end;

procedure TMonitorForm.Timer1Timer(Sender: TObject);
begin
  //确保出现Tray图标
  AddIconToTray;
end;

procedure TMonitorForm.WndProc(var Msg: TMessage);
begin
  inherited;
  // 响应任务栏托盘重建消息
  if Msg.Msg = MsgTaskbarRestart then
    AddIconToTray; // 确保出现Tray图标
end;
end.
