object SettingForm: TSettingForm
  Left = 319
  Top = 311
  Width = 460
  Height = 154
  Caption = #35774#32622#31995#32479#21442#25968
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 452
    Height = 120
    Align = alClient
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 112
      Top = 87
      Width = 75
      Height = 25
      Caption = #30830#23450
      TabOrder = 1
      OnClick = BitBtn1Click
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 232
      Top = 87
      Width = 75
      Height = 25
      Cancel = True
      Caption = #20851#38381
      ModalResult = 2
      TabOrder = 2
      OnClick = BitBtn2Click
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 450
      Height = 72
      Align = alTop
      Caption = #31995#32479#21442#25968
      Color = clBtnFace
      ParentColor = False
      TabOrder = 0
      DesignSize = (
        450
        72)
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 48
        Height = 12
        Caption = #31243#24207#21517#31216
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 48
        Height = 12
        Caption = #26381#21153#25551#36848
      end
      object Edit1: TEdit
        Left = 64
        Top = 16
        Width = 377
        Height = 20
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
      end
      object Edit2: TEdit
        Left = 64
        Top = 40
        Width = 377
        Height = 20
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
      end
    end
  end
end
