unit WefeelSMSDLL;


interface
  procedure SMSCreate();stdcall;external 'WefeelSMS.dll';
  procedure SMSSetting(ComNumber:integer;Baud:integer);stdcall;external 'WefeelSMS.dll';
  procedure SMSStart();stdcall;external 'WefeelSMS.dll';
  procedure SMSStop();stdcall;external 'WefeelSMS.dll';
  procedure SMSFree();stdcall;external 'WefeelSMS.dll';
  procedure SMSPutSMS(pCode,pContent:PChar;id:integer);stdcall;external 'WefeelSMS.dll';
  function SMSGetSMS(pCode,pTime,pContent:PChar):boolean;stdcall;external 'WefeelSMS.dll';
  function SMSGetLine(pLine:PChar):boolean;stdcall;external 'WefeelSMS.dll';
  function SMSQueueLength():integer;stdcall;external 'WefeelSMS.dll';
  procedure SMSSettingWindow();stdcall;external 'WefeelSMS.dll';
  procedure SMSSetSendSpeed(Speed:integer);stdcall;external 'WefeelSMS.dll';
  function SMSGetSendSpeed():integer;stdcall;external 'WefeelSMS.dll';
  function SMSGetState(out Id,State:integer):boolean;stdcall;external 'WefeelSMS.dll';
implementation

{uses ActiveX;

Initialization
  CoInitialize(nil);
finalization
  CoUnInitialize;}

end.
 