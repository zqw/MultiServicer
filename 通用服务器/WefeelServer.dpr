program WefeelServer;

uses
  SvcMgr,
  Forms,
  Windows,
  SysUtils,Dialogs,StrUtils,    
  MyAbout in 'MyAbout.pas' {MyAboutBox},
  SettingUnit in 'SettingUnit.pas' {SettingForm},
  ServiceUnit in 'ServiceUnit.pas' {WefeelService: TWefeelService},
  WinService in 'WinService.pas',
  WinSvcEx in 'WinSvcEx.pas',
  InstallUnit in 'InstallUnit.pas' {InstallForm},
  MonitorUnit in 'MonitorUnit.pas' {MonitorForm},
  ServerPlugin in 'ServerPlugin.pas',
  AutoPlugin in 'AutoPlugin.pas';

{$R *.res}
{$Message Hint '提示：修改了DOG定义后需要重新Build项目。'}

{$IF DEFINED(DOG)}
//{$R WefeelServerWithDog.res}
{$Message Hint '请注意：此次编译生成的程序需要软件狗！'}
{$ELSE}
{$Message Hint '请注意：此次编译生成的程序不需要软件狗。'}
//{$Message Hint '请注意：如果需要软件狗，请在Project->Options->Conditionals中增加DOG定义。'}
{$IFEND}

const
  CSMutexName = 'Global\WefeelServer_Mutex';
var
  OneInstanceMutex: THandle;
  SecMem: SECURITY_ATTRIBUTES;
  aSD: SECURITY_DESCRIPTOR;
begin

  SetCurrentDir(ExtractFileDir(Application.ExeName));
  if RightStr(LowerCase(ParamStr(1)),7)='install' then//带参数则是从服务运行
  begin
    Forms.Application.Initialize;
    Forms.Application.Title := '通用服务管理';
  Forms.Application.CreateForm(TInstallForm, InstallForm);
    Forms.Application.Run;
  end
  else
  begin
    //防止重复运行
    InitializeSecurityDescriptor(@aSD, SECURITY_DESCRIPTOR_REVISION);
    SetSecurityDescriptorDacl(@aSD, True, nil, False);
    SecMem.nLength := SizeOf(SECURITY_ATTRIBUTES);
    SecMem.lpSecurityDescriptor := @aSD;
    SecMem.bInheritHandle := False;
    OneInstanceMutex := CreateMutex(@SecMem, False, CSMutexName);
    if (GetLastError = ERROR_ALREADY_EXISTS)then
    begin
      ShowMessage('程序已经在后台!不能重复运行！');
      Exit;
    end;

    if ParamCount=0 then
    begin //不带参数则
      Forms.Application.Initialize;
      Forms.Application.Title := '通用服务器';
      Forms.Application.CreateForm(TMonitorForm, MonitorForm);
      Forms.Application.Run;
    end
    else
    begin
      if RightStr(LowerCase(ParamStr(1)),7)='service' then//带参数则是从服务运行
      begin
        SvcMgr.Application.Initialize;
        Forms.Application.Title:= '通用服务器';
        SvcMgr.Application.CreateForm(TWefeelService, WefeelService);
        SvcMgr.Application.Run;
      end;
    end;
  end;
end.
