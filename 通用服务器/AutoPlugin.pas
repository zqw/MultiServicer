{
自动加载放在插件目录下的符合规范的dll，形成菜单供选择
}
unit AutoPlugin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls;

type
  //定义接口函数类型
  TShowForm = function(): boolean; stdcall; //窗体显示
  TGetCaption = function(): pchar; stdcall; //取标题,用于菜单项

  //定义B类自动插件,存放 Caption、Address,Call信息
  TAutoPlugin = Class
     Caption: string; //存取加载后的DLL中GetCaption返回的标题
     Address: THandle; //存取加载后的DLL的句柄
     Call: Pointer; //存取ShowDllForm函数的句柄,指针类型
  end;

  procedure LoadPlugins(menu:TMenuItem); //初始化插件（装入插件，并在菜单提供调用）
  procedure FreePlugins; //释放插件
  //procedure PluginsClick(Sender:TObject); //插件菜单点击事件
  procedure ClickPlugins(Sender:TObject);

var
  ShowForm: TShowForm; //声明接口函数类型
  Plugins: TList; //指针列表,存放每一个DLL加载后的相关信息
  StopSearch: boolean;

implementation

//通用过程，查找指定的文件，并存于Files中
procedure SearchFileExt(const Dir, Ext: string; Files: TStrings);
var
   found: TSearchRec;
   i: integer;
   Dirs: TStrings;
   Finished: integer;
begin
  StopSearch := false;
  Dirs := tstringlist.create;
  finished := findfirst(dir+'*.*', 63, Found);
  while (finished=0) and not (StopSearch) do
  begin
    if (Found.Name[1]<>'.') then
    begin
      if (found.attr and faDirectory=faDirectory) then
        dirs.add(dir+found.name) //加入到目录列表
      else
        if POS(UpperCase(Ext), UpperCase(found.name))>0 then
          files.add(dir+found.name);
    end;
    finished := findnext(found);
  end;
  findclose(found);
  if not StopSearch then
    for i:=0 to dirs.count-1 do
      searchfileext(Dirs[i], Ext, Files);
  Dirs.Free;
end;

//装载插件
procedure LoadPlugins(menu:TMenuItem);
var
  files: tstrings; //存放文件查找结果的文件列表
  i: integer;
  NewMenu:TMenuItem;
  MyPlugin: TAutoPlugin; //存放插件信息的自定义的变量
  GetCaption: TGetCaption; //获取插件标题的过程引用
begin
  files := tstringlist.Create; //文件列表
  Plugins := tlist.Create; //建立指针列表
  //查找当前目录的子目录plugins下的 .dll文件，并存于 "files 文件列表"中
  SearchFileExt(Extractfilepath(application.ExeName)+'扩展\','.dll',files);
  //从文件列表中加载找到的DLL
  for i:=0 to files.Count-1 do
  begin
    myPlugin := TAutoPlugin.Create;
    myPlugin.Address := loadlibrary(pchar(files[i])); //装载DLL，返回句柄
    if myplugin.Address=0 then
      showmessage('加载'+files[i]+'失败!')
    else begin
      try
        @GetCaption:=nil;
        @GetCaption:=GetProcAddress(myPlugin.Address,'GetCaption'); //通过DLL句柄取得“获取插件标题”过程的入口地址
        if @GetCaption<>nil then
        begin
          myPlugin.Caption:=GetCaption(); //返回插件标题
          myPlugin.Call:=GetProcAddress(myPlugin.Address,'ShowForm'); //获取并保存窗体显示过程的入口地址
          Plugins.Add(myPlugin); //加入至指针列表
          //创建菜单，并将菜单标题OnClick事件赋值
          NewMenu:=TMenuItem.Create(nil);
          NewMenu.Caption:=myplugin.Caption;
          NewMenu.OnClick:=menu.OnClick; //绑定事件
          NewMenu.Tag:=i; //用此标识作为插件事件的区分
          menu.Add(newMenu); //加入菜单项
        end;
      except
        showmessage('初始化失败!');
        raise;
      end;
    end;
  end;
  menu.OnClick:=nil;
  files.Free;
end;

procedure FreePlugins;
var
  i: integer;
begin
  for i:=0 to plugins.Count-1 do
  begin
    freelibrary(TAutoPlugin(plugins[i]).Address); //按DLL的句柄释放内存
  end;
  plugins.free;
end;

//插件菜单项点击事件
procedure ClickPlugins(Sender:TObject);
begin
  //根据菜单项的TAG属性对应函数调用的地址
  @ShowForm := TAutoPlugin(plugins[TMenuItem(Sender).Tag]).Call;
  //执行ShowDllForm函数
  try
    ShowForm();
  except
    showmessage('打开窗体错误!');
  end;
end;

end.
