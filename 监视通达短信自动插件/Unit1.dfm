object Form1: TForm1
  Left = 235
  Top = 216
  Width = 604
  Height = 346
  Caption = #30417#35270#36890#36798#20018#21475#30701#20449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object Memo1: TMemo
    Left = 0
    Top = 41
    Width = 596
    Height = 271
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 596
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 90
      Height = 15
      Caption = #26381#21153#22120#22320#22336#65306
    end
    object Label2: TLabel
      Left = 192
      Top = 16
      Width = 45
      Height = 15
      Caption = #31471#21475#65306
    end
    object Edit1: TEdit
      Left = 88
      Top = 8
      Width = 97
      Height = 23
      TabOrder = 0
      Text = '127.0.0.1'
    end
    object Button1: TButton
      Left = 280
      Top = 8
      Width = 75
      Height = 25
      Caption = #36830#25509
      TabOrder = 1
      OnClick = Button1Click
    end
    object Edit2: TEdit
      Left = 232
      Top = 8
      Width = 41
      Height = 23
      TabOrder = 2
      Text = '7731'
    end
    object ComboBox1: TComboBox
      Left = 368
      Top = 8
      Width = 97
      Height = 23
      ItemHeight = 15
      ItemIndex = 0
      TabOrder = 3
      Text = 'start'
      Items.Strings = (
        'start'
        'stop')
    end
    object Button2: TButton
      Left = 472
      Top = 8
      Width = 75
      Height = 25
      Caption = #21457#36865#21629#20196
      TabOrder = 4
      OnClick = Button2Click
    end
  end
  object ClientSocket1: TClientSocket
    Active = False
    Address = '10.18.68.7'
    ClientType = ctNonBlocking
    Port = 7731
    OnConnect = ClientSocket1Connect
    OnDisconnect = ClientSocket1Disconnect
    OnRead = ClientSocket1Read
    Left = 312
    Top = 104
  end
end
