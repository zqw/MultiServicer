unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ScktComp, ExtCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    ClientSocket1: TClientSocket;
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Label2: TLabel;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    Button2: TButton;
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    procedure Button1Click(Sender: TObject);
    procedure ClientSocket1Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ClientSocket1Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function ShowForm():boolean;stdcall;
function GetCaption():PChar;stdcall;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function ShowForm():boolean;stdcall;
begin
  if not Assigned(Form1) then
    Form1:=TForm1.Create(nil);
  {Form1.ShowModal;
  Form1.Free;
  Form1:=nil;}
  Form1.Show;
  Result:=true;
end;

function GetCaption():PChar;stdcall;
begin
  Result:='监控通达OA短信收发';
end;

procedure TForm1.ClientSocket1Read(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  Memo1.Lines.Add(Socket.ReceiveText);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if ClientSocket1.Active=false then
  begin
    ClientSocket1.Address:=Edit1.Text;
    ClientSocket1.Port:=StrToInt(Edit2.Text);
    ClientSocket1.Open;
  end
  else
  begin
    ClientSocket1.Close;
  end;

end;

procedure TForm1.ClientSocket1Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  Button1.Caption:='断开';
  Edit1.Enabled:=false;
  Edit2.Enabled:=false;
  Socket.SendText('iamzong');
end;

procedure TForm1.ClientSocket1Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  Button1.Caption:='连接';
  Edit1.Enabled:=true;
  Edit2.Enabled:=true;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  ClientSocket1.Socket.SendText(ComboBox1.Text);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ClientSocket1.Close;
  Form1.Free;
  Form1:=nil;
end;

end.
